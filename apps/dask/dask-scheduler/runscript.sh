export PATH="/global/homes/h/hkrishna/camera/conda/bin:$PATH"
export LD_LIBRARY_PATH="/global/homes/h/hkrishna/camera/conda/lib:$LD_LIBRARY_PATH"

echo "activating xicam scheduler", $1, $2

ipaddr=$1
port=$2

echo dask-scheduler --host $ipaddr --port $port
dask-scheduler --host $ipaddr --port $port --no-bokeh --no-show --http-port 0 &
echo dask-worker $ipaddr:$port --nprocs 1 --nthreads 1
dask-worker $ipaddr:$port --nprocs 1 --nthreads 1

