from distributed import Client, LocalCluster
from distributed import Scheduler
from tornado.ioloop import IOLoop
from threading import Thread

loop = IOLoop.current()
t = Thread(target=loop.start)
t.daemon = True
t.start()

import comm_interface

cluster = None
def main():
    global cluster
    cluster = LocalCluster(n_workers=1)
    print cluster.scheduler.port

    comm_interface.global_comm_task.setup("dask-cluster", wait=False)
    comm_interface.global_comm_task.broadcast(cluster.scheduler.port)

    import time
    while True:
      time.sleep(1)


if __name__ == "__main__":
    main()
