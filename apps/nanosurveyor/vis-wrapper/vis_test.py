import PyQt4
from PyQt4 import QtGui
from PyQt4 import QtCore

from PyQt4.QtGui import *
from PyQt4.QtCore import *

import zmq

from comm_interface import global_comm_task
import numpy as np

from pyqtgraph.Qt import QtCore, QtGui
import pyqtgraph as pg


def terminate_application_callback():
  QtCore.QCoreApplication.instance().quit()

# Interpret image data as row-major instead of col-major
pg.setConfigOptions(imageAxisOrder='row-major')

app = QtGui.QApplication([])

class Window(QtGui.QWidget):
    my_signal = pyqtSignal(str)

    def __init__(self, imv, imv2):
        QtGui.QWidget.__init__(self)
        layout = QtGui.QHBoxLayout(self)
        self.edit = QtGui.QTextEdit()
        self.edit.setFixedWidth(200)
        layout.addWidget(self.edit)
        layout.addWidget(imv)
        layout.addWidget(imv2)
        self.my_signal.connect(self.appendText)

    @pyqtSlot(str)
    def appendText(self, text):
        self.edit.append(text)

imv = pg.ImageView()
imv2 = pg.ImageView()
imv2.setLevels(0, 255)
win = Window(imv, imv2)
win.resize(1500,800)
win.show()
win.setWindowTitle('RawView')

#win2 = QtGui.QMainWindow()
#win2.resize(800,800)
#imv2 = pg.ImageView()
#win2.setCentralWidget(imv2)
#win2.show()
#win2.setWindowTitle('ReconView')

#imv.setImage(data, xvals=np.linspace(1., 3., data.shape[0]))

## Set a custom color map
colors = [
    (0, 0, 0),
    (45, 5, 61),
    (84, 42, 55),
    (150, 87, 60),
    (208, 171, 141),
    (255, 255, 255)
]

cmap = pg.ColorMap(pos=np.linspace(0.0, 1.0, 6), color=colors)
#imv.setColorMap(cmap)


class Vis:
  def __init__(self):
      pass

def on_recv(message):
  import pickle
  datax = pickle.loads(message[0])

  if datax["command"] == "frame":
    datax = datax["data"]
    print datax["data"].shape
    imageData = datax["data"]
    win.my_signal.emit("nsptycho: " + str(datax.keys()))
    try:
      imv.setImage(imageData) # , xvals=np.linspace(1., 2., data.shape[0]))
    except:
      pass
  elif datax["command"] == "recon":
    datax = datax["data"]
    print datax["data"].shape
    imageData = datax["data"]
    try:
      imv2.setImage(imageData, autoLevels=False) # , xvals=np.linspace(1., 2., data.shape[0]))
    except:
      pass
  elif datax["command"] == "log":
    # print datax["data"]
    # win.appendText(datax["data"])
    win.my_signal.emit(datax["data"])


def recv_callback():

  while global_comm_task.has_activity():
    data = global_comm_task.recv()
    # print data
    a = data[0]
    b = data[1]

    if isinstance(b, dict) and "command" in b:
        if b["command"] == "frame":
            #print b["data"]["data"].shape
            my_data = b["data"]["data"]

            try:
                imv.setImage(my_data)
            except:
                pass

        if b["command"] == "recon":
            try:
                #print "RECON"
                my_data = b["data"]["data"]
                med = np.median(my_data)
                imv2.setImage(my_data, levels=(0.9*med, 1.1*med))
            except:
                pass




def main():
  vis = Vis()
  global_comm_task.setup("vis", wait=False, terminate_callback=terminate_application_callback)

if __name__ == "__main__":
  main()
  import sys

  timer = QtCore.QTimer()
  timer.timeout.connect(recv_callback)
  timer.start()

  if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
        QtGui.QApplication.instance().exec_()

