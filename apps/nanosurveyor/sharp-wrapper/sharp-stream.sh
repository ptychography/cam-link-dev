#!/bin/bash

module load sharp
module load pyzmq

PYTHON_CMD=$1
EXEC_PATH=$2
LOCAL_PYTHON_COMMAND=$3

$PYTHON_CMD $EXEC_PATH/sharp-stream.py
#mpirun -n 4 $LOCAL_PYTHON_COMMAND $EXEC_PATH/sharp-worker.py
#$LOCAL_PYTHON_COMMAND $EXEC_PATH/sharp-worker.py
