import comm_interface
import zmq
import pickle

def get_free_local_port():
    s = socket.socket()
    s.bind(("", 0))
    local_port = s.getsockname()[1]
    s.close()
    return local_port

if __name__ == "__main__":
    comm_interface.global_comm_task.setup("sharp", wait=False)

    ctx = zmq.Context()
    pub_socket = ctx.socket(zmq.PUB)
    #free_port = get_free_local_port()
    pub_socket.bind("tcp://*:15000")

    sub_socket = ctx.socket(zmq.SUB)
    sub_socket.bind("tcp://*:9500")
    sub_socket.setsockopt(zmq.SUBSCRIBE, "")

    zmq_poller = zmq.Poller()
    zmq_poller.register(sub_socket, zmq.POLLIN)

    while True:
      if comm_interface.global_comm_task.has_activity():
          msg = comm_interface.global_comm_task.recv()
          #data_tag = msg[0]
          #print msg
          data_msg = msg[1]

          if isinstance(data_msg, str):
              #print "data pickling..", data_msg
              data_msg = pickle.loads(data_msg)
              #print "pickled", data_msg

          try:
            pickle_string = pickle.dumps(data_msg)
            pub_socket.send(pickle_string)
          except:
            print "not sent"
          #comm_interface.global_comm_task.send(str("msg recv from source"))
          #data_msg["command"] = "recon"
          #comm_interface.global_comm_task.send(data_msg)

      if sub_socket in dict(zmq_poller.poll(1)):
          #print "sub socket got.."
          data_msg = sub_socket.recv()
          data_pickle = pickle.loads(data_msg)
          comm_interface.global_comm_task.send(data_pickle) 
