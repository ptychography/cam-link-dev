#!/bin/bash -l

#SBATCH -p batch
#SBATCH -w n0004
#SBATCH -N 1
#SBATCH -n 4

module load gcc openmpi python mpi4py
srun python -c "from mpi4py import *;print MPI.COMM_WORLD.Get_rank()"
srun hostname
mpirun python -c "from mpi4py import *;print MPI.COMM_WORLD.Get_rank()"

