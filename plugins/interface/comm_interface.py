import zmq
import threading
import glob
import os
import time
import json
try:
    import cPickle as pickle
except:
    import pickle


class CommTask:
    def __init__(self):
        self.initialized = False
        self.connected = False
        self.socket = None
        self.address = None
        self.port = None
        self.name = None
        self.input_vars = None
        self.output_vars = None
        self.target = None
        self.buffer_input = True
        self.buffer_output = True
        self.input_conns = []
        self.output_conns = []
        self.terminate_callback = None
        self.recv_callback = None
        self.start_network = False
        self.broadcast_data = []
        self.master_poller = zmq.Poller()

    def setup(self,
              name,
              input_vars=None,
              output_vars=None,
              buffer_input=True,
              buffer_output=True,
              terminate_callback=None,
              recv_callback=None,
              wait=True,
              wait_for_connect=True,
              ):

        self.name = name
        self.input_vars = input_vars
        self.output_vars = output_vars
        self.buffer_input = buffer_input
        self.buffer_output = buffer_output
        self.terminate_callback = terminate_callback
        self.recv_callback = recv_callback
        self.start_network = False

        self.target = threading.Thread(target=self.load_service)
        self.target.daemon = True
        self.target.start()

        if wait is True:
            while self.initialized is False:
                time.sleep(1)

        if wait_for_connect is True:
            while self.initialized is False:
                time.sleep(1)

        while self.start_network is False:
            time.sleep(1)

    @staticmethod
    def get_ip_address(ifname="eth0"):
        import socket
        import fcntl
        import struct

        print "in here"

        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        return socket.inet_ntoa(fcntl.ioctl(
            s.fileno(),
            0x8915,  # SIOCGIFADDR
            struct.pack('256s', ifname[:15])
        )[20:24])

    @staticmethod
    def get_free_local_port():
        """
        :return:
        """
        import socket
        s = socket.socket()
        s.bind(("", 0))
        local_port = s.getsockname()[1]
        s.close()
        return local_port

    def load_service(self):

        while not self.initialized:
            path = os.path.expanduser("~/.camera/")
            results = glob.glob(path + "/active_service_*")

            if len(results) > 0:
                self.init(results[0])

            # sleep for 2 seconds before trying again
            time.sleep(2)

        context = zmq.Context()
        self.socket = context.socket(zmq.DEALER)
        self.socket.connect("tcp://" + self.address + ":" + str(self.port))

        poller = zmq.Poller()
        poller.register(self.socket, zmq.POLLIN)

        self.send_ctrl("__service__", -1,
                       dict(name=self.name, input_vars=self.input_vars, output_vars=self.output_vars))
        counter = 0

        while True:
            if self.socket in dict(poller.poll(10)):
                counter = 0  # any command sets counter to 0
                # print "in here.."
                self.connected = True
                msg = self.socket.recv_multipart()

                try:
                    if type(msg) is list or type(msg) is tuple:
                        if len(msg) > 1:
                            msg = msg[1]
                        else:
                            msg = msg[0]

                    result = json.loads(msg)

                    if result["command"] == "__success__":
                        print("SUCCESS connection...")

                    if result["command"] == "__command__":
                        self.process_command(result)

                    if result["command"] == "__keep_alive__":
                        print("Here connection...")

                    if result["command"] == "__quit__":
                        print("Terminating connection...")
                        self.send_ctrl("__confirm_quit__", -1, "Confirmed")

                        # terminate if callback is available..
                        if self.terminate_callback is not None:
                            self.terminate_callback()
                        break

                except:
                    print("could not parse", str(msg))
                    pass
            else:
                counter = counter + 1

                #if counter % 500 == 0:
                #    print counter
                #    self.send_ctrl("__keep_alive__", -1, dict())

                # if no action
                #if counter == 5000:
                #    # if termination signal is set then call it...
                #    if self.terminate_callback is not None:
                #        self.terminate_callback()
                #    break
                pass

        # break connection with remote process
        # but let application decide when to terminate additional connections
        self.socket.close()
        context.term()

    def send_ctrl(self, tag, uid, msg):
        response = dict()
        response["command"] = tag
        response["output"] = msg
        response["uid"] = uid
        print("sending: ", json.dumps(response))
        self.socket.send(json.dumps(response))

    def process_command(self, result):
        output = result["output"]
        uid = output[0]
        command = output[1]
        task = command["task"]
        actions = command["comm"]

        print task, actions, isinstance(actions, str), isinstance(actions, unicode)
        response = []
        try:
            if isinstance(actions, str) or isinstance(actions, unicode):
                if actions == "start":
                    print "starting network..."
                    self.start_network = True
                if actions == "config_data":
                    response.append(self.broadcast_data)
            elif isinstance(actions, list):
                for action in actions:
                    if isinstance(action, dict) and "type" in action:
                        comm_type = action["type"]
                        freq = 1

                        if "freq" in action:
                            freq = int(action["freq"])

                        # write data during send operations...
                        if comm_type == "publish":
                            if "port" in action:
                                publish_port = int(action["port"])
                            else:
                                publish_port = self.get_free_local_port()
                            ctx = zmq.Context()
                            socket = ctx.socket(zmq.PUB)
                            socket.bind("tcp://*:" + str(publish_port))
                            self.output_conns.append([socket, freq, 0])
                            response.append(publish_port)

                        if comm_type == "push":
                            if "port" in action:
                                push_port = int(action["port"])
                            else:
                                push_port = self.get_free_local_port()
                            ctx = zmq.Context()
                            socket = ctx.socket(zmq.PUSH)
                            socket.bind("tcp://*:" + str(push_port))
                            self.output_conns.append([socket, freq, 0])
                            response.append(push_port)

                        if comm_type == "pull":
                            ctx = zmq.Context()
                            ip = action["ip"]
                            port = action["port"]
                            socket = ctx.socket(zmq.PULL)
                            socket.connect("tcp://" + str(ip) + ":" + str(port))
                            self.input_conns.append(socket)

                        if comm_type == "subscribe":
                            ctx = zmq.Context()
                            ip = action["ip"]
                            port = action["port"]
                            socket = ctx.socket(zmq.SUB)
                            socket.connect("tcp://" + str(ip) + ":" + str(port))
                            socket.setsockopt(zmq.SUBSCRIBE, "")
                            self.input_conns.append(socket)

        except:
            pass

        for conn in self.input_conns:
            # print "registering " + str(conn)
            self.master_poller.register(conn, zmq.POLLIN)

        print("CONNECTIONS: input: ", len(self.input_conns), "output: ", len(self.output_conns))

        # print(uid, task, actions)
        # print("Command is,", str(result))
        self.send_ctrl("__command_response__", -1, [uid, task, response])

    def init(self, path):
        results = open(path)
        contents = results.readlines()
        contents = [c.strip() for c in contents]
        print("Contents = " + str(contents))

        port = contents[0].split("=")
        address = contents[1].split("=")
        self.port = int(port[1].strip())
        self.address = address[1].strip()
        print("found address = ", self.port, self.address)
        self.initialized = True

    def broadcast(self, data):
        print "BROADCAST:", data
        self.broadcast_data.append(data)
        print self.broadcast_data

    def send(self, data):
        for i, conn in enumerate(self.output_conns):
            conn[2] = conn[2] + 1

            # print (str(conn[2]), str(conn[1]))
            if conn[2] >= conn[1]:  # only send data that matches on frequency of occurrence
                conn[2] = 0

                try:
                    pickle_string = pickle.dumps(data)
                    conn[0].send(pickle_string)
                except:
                    conn[0].send("No data")
                    pass

    def has_activity(self, timeout=1):
        # print "checking for activity = ", len(self.input_conns)
        for conn in self.input_conns:
            if conn in dict(self.master_poller.poll(timeout)):
                return True

    def recv(self, blocking=False):
        if blocking is False and not self.has_activity():
            # print "NO ACTIVITY"
            return []

        # if blocking is true then loop till there is activity
        while not self.has_activity():
            time.sleep(1)

        # result = []
        for i, conn in enumerate(self.input_conns):
            if conn in dict(self.master_poller.poll(1)):
                msg = conn.recv()
                data = msg

                try:
                    #print data
                    data = pickle.loads(msg)
                except:
                    print "pickle failed to load"

                #print ("message recv: ", len(msg), len(result))
                return [i, data]

global_comm_task = CommTask()