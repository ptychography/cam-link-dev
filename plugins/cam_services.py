import os, sys
sys.path.append(os.path.dirname(os.path.dirname(__file__)))
# print(os.path.dirname(__file__))

from plugins.task_interface import *

"""
Register Core Services
"""


class InstallerCommand(Task):
    def name(self):
        return "Install"

    def input_vars(self):
        return [("package", "str")]

    def output_vars(self):
        return [("result", "str")]

    def initialize(self, args):
        pass

    def execute(self, args=None):
        import shlex
        import subprocess
        command = "../conda/bin/conda install -y " + str(args["package"])
        result = subprocess.check_output(shlex.split(command))
        return result

    def terminate(self):
        pass


class ExecuteCommand(Task):
    def name(self):
        return "Execute"

    def input_vars(self):
        return [("command", "str")]

    def output_vars(self):
        return [("output", "str")]

    def initialize(self, args):
        pass

    def execute(self, args=None):
        import shlex
        import subprocess

        command = args["command"]
        result = subprocess.check_output(shlex.split(command))
        return result

    def terminate(self):
        pass

