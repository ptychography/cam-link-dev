import json


class Task:
    def __init__(self):
        pass

    # Override functions

    def name(self):
        return ""

    def initialize(self, args):
        pass

    def input_vars(self):
        return []

    def output_vars(self):
        return []

    def terminate(self):
        pass

    def execute(self, args=None):
        pass

    def query(self, data):
        pass

    def response(self, data):
        pass


class RunningTask(Task):
    def __init__(self, input_data, worker=None, service=None):
        self.input = input_data
        self.worker = worker  # communicate with client
        self.service = service  # communicate with outside world
        self.state = "Initialized"
        self.exec_process = []
        self.identity = None

        if worker is not None or service is not None:
            self.state = "Running"

    def set_running_status(self, worker, service):
        self.worker = worker
        self.service = service
        self.state = "Running"

    # Override functions

    def name(self):
        return self.input["name"]

    def initialize(self, args):
        pass

    def input_vars(self):
        return ["", ""]

    def execute(self, args=None):
        import os
        import subprocess
        import shlex

        num_tasks = int(self.input["num_tasks"])
        command = self.input["executable"]

        self.exec_process = []
        for i in range(num_tasks):
            print "Launching Command: " + command

            my_env = os.environ.copy()
            npath = os.path.dirname(__file__) + "/interface"
            mpath = os.path.dirname(os.path.dirname(__file__)) + "/src"

            if "PYTHONPATH" in my_env:
                my_env["PYTHONPATH"] = mpath + ":" + npath + ":" + my_env["PYTHONPATH"]
            else:
                my_env["PYTHONPATH"] = mpath + ":" + npath

            print shlex.split(command)

            p = subprocess.Popen(shlex.split(command), env=my_env)
            self.exec_process.append(p)
            self.state = "Started"

    def output_vars(self):
        return ["", ""]

    def terminate(self):
        print("sending terminate command")
        try:
            if self.worker is not None:
                self.send_ctrl("__quit__", self.identity, "Quit application")

                try:
                    self.worker.recv(5)
                except:
                    print("Message timed out...")
                    pass

                print("Confirmation received...")

        except:
            pass

        try:
            for p in self.exec_process:
                p.terminate()
        except:
            pass

    def process(self, uid, comm):
        self.send_ctrl("__command__", self.identity, [uid, comm])

    def send(self, data):
        self.worker.send(data)
        pass

    def handle_recv(self, data):
        print "DATA COMMAND", data["command"], self.name()

        if data["command"] == "__command_response__":
            datax = data["output"]
            uid = datax[0]
            task = datax[1]
            msg = datax[2]
            response = dict(task=task, comm=msg)

            self.service.send_data(uid, "response", response)

        if data["command"] == "__broadcast__":
            datax = data["output"]
            uid = datax[0]
            task = datax[1]
            msg = datax[2]
            response = dict(task=task, comm=msg)

            self.service.send_data(uid, "response", response)

    def send_ctrl(self, tag, identity, msg):
        import zmq
        response = dict()
        response["command"] = tag
        response["output"] = msg
        print("sending: ", json.dumps(response))
        self.worker.send_multipart([identity, json.dumps(response)])
        #self.worker.setsockopt(zmq.IDENTITY, self.identity)
        #self.worker.send(json.dumps(response))
        ##self.worker.identity


class ServiceTask(Task):
    def __init__(self, service, parent):
        self.service = service
        self.pub_socket = None
        self.sub_socket = None
        self.pub_stream = None
        self.sub_stream = None
        self.publish_port = None
        self.subscribe_port = None
        self.parent = parent
        self.process = []
        self.initialized_count = 0

    @staticmethod
    def get_free_local_port():
        import socket
        s = socket.socket()
        s.bind(("", 0))
        local_port = s.getsockname()[1]
        s.close()
        return local_port

    def name(self):
        return self.service["name"]

    def input_vars(self):
        return zip(self.service["input"], self.service["input_types"])

    def output_vars(self):
        return zip(self.service["output"], self.service["output_types"])

    def on_recv_data(self, message):
        message = message[0]

        if message == "__keep_alive__":
            self.send_data("__keep_alive__")
            return

        if str(message).find("initialized") >= 0:
            print message
            self.initialized_count += 1

    def send_data(self, message):
        self.pub_stream.send(message)

    def initialize(self, args):
        # launch service pass data
        import subprocess
        import shlex
        import os
        import zmq
        from zmq.eventloop.zmqstream import ZMQStream
        from zmq.eventloop import ioloop

        print "Executing service: ", self.name()

        ctx = zmq.Context()

        self.publish_port = ServiceTask.get_free_local_port()
        self.pub_socket = ctx.socket(zmq.PUB)
        self.pub_socket.bind("tcp://*:%s" % self.publish_port)
        self.pub_stream = ZMQStream(self.pub_socket)

        subscribe_name = "localhost"
        if "subscribe_name" in args and args["subscribe_name"] is not None:
            subscribe_name = str(args["subscribe_name"])

        self.subscribe_port = ServiceTask.get_free_local_port()
        self.sub_socket = ctx.socket(zmq.SUB)
        self.sub_socket.connect("tcp://" + subscribe_name + ":" + str(self.subscribe_port))
        self.sub_stream = ZMQStream(self.sub_socket)
        self.sub_stream.setsockopt(zmq.SUBSCRIBE, "")

        print("Publishing : ", self.publish_port, " ", self.subscribe_port)

        self.sub_stream.on_recv(self.on_recv_data)

        num_tasks = self.service["num_tasks"]

        # command = "executable" + " " + "--cam-token" + "0:sub_port:pub_port", other params
        input_port = "-1"
        output_port = "-1"

        if "input_port" in args and args["input_port"] is not None:
            input_port = str(args["input_port"])

        if "output_port" in args and args["output_port"] is not None:
            output_port = str(args["output_port"])

        self.process = []
        for i in range(num_tasks):
            token = str(i) + ":" + str(self.publish_port) + ":" + str(self.subscribe_port)
            token += ":" + input_port + ":" + output_port
            params = self.service["parameters"]
            command = self.service["executable"] + " " + "--cam-token " + token + " " + params

            print "Launching Command: " + command

            my_env = os.environ.copy()

            npath = os.path.dirname(__file__) + "/interface"
            mpath = os.path.dirname(os.path.dirname(__file__)) + "/src"

            if "PYTHONPATH" in my_env:
                my_env["PYTHONPATH"] = mpath + ":" + npath + ":" + my_env["PYTHONPATH"]
            else:
                my_env["PYTHONPATH"] = mpath + ":" + npath

            my_env["CAM_TOKEN"] = token

            print shlex.split(command)

            p = subprocess.Popen(shlex.split(command), env=my_env, preexec_fn=os.setsid)
            self.process.append(p)

        if "input_port" in args and args["input_port"] is not None:
            self.pub_stream.send("input_port:" + str(args["input_port"]))

        if "output_port" in args and args["output_port"] is not None:
            self.pub_stream.send("output_port:" + str(args["output_port"]))

        while self.initialized_count < len(self.process):
            import time
            time.sleep(1)

        print "Finished initializing..."
        for p in self.process:
            p.wait()

    def execute(self, args=None):
        self.pub_stream.send("execute")

    def terminate(self):
        import os
        import signal

        self.send_data("terminate")
        self.pub_stream.flush()

        for p in self.process:
            try:
                p.terminate()
                pgrp = os.getpgid(p.pid)
                os.killpg(pgrp, signal.SIGINT)
            except:
                pass
