import os
import sys
import fnmatch
import signal

import threading

import zmq
from zmq.eventloop import ioloop
from zmq.eventloop.zmqstream import ZMQStream
import importlib
import SocketServer

sys.path.append(os.path.dirname(os.path.dirname(__file__)))
print(os.path.dirname(__file__))

from plugins.task_interface import *

"""
Each node has a service that runs tasks.
"""


class RemoteService:
    class RunObject:

        def __init__(self, task, parent, uid):
            self.task = task
            self.parent = parent
            self.uid = uid

        def terminate_func(self):
            self.task.terminate()

        def initialize_func(self, data):
            try:
                ret = self.task.initialize(data)
                self.parent.send_data(self.uid, "response", ret)
            except Exception as ex:
                self.parent.send_data(self.uid, "error in execution" + repr(ex))
            self.parent.owned_tasks.remove(self)

        def run_func(self, data=None):
            try:
                ret = self.task.execute(data)
                self.parent.send_data(self.uid, "response", ret)
            except Exception as ex:
                self.parent.send_data(self.uid, "error in execution" + repr(ex))
            self.parent.owned_tasks.remove(self)

    class ForwardServer(SocketServer.ThreadingTCPServer):
        """
        Class to support Forwarding through Daemon Threads
        """
        daemon_threads = True
        allow_reuse_address = True

    class Handler(SocketServer.BaseRequestHandler):

        def setup(self):
            print("Handler setup called")

        # noinspection PyUnresolvedReferences
        def handle(self):
            import select
            try:
                chan = self.ssh_transport.open_channel('direct-tcpip',
                                                       (self.chain_host, self.chain_port),
                                                       self.request.getpeername())
            except Exception as e:
                print('Incoming request to %s:%d failed: %s' % (self.chain_host,
                                                                self.chain_port,
                                                                repr(e)))
                return

            if chan is None:
                print('Incoming request to %s:%d was rejected by the SSH server.' %
                      (self.chain_host, self.chain_port))
                return

            print('Connected!  Tunnel open %r -> %r -> %r' % (self.request.getpeername(),
                                                              chan.getpeername(),
                                                              (self.chain_host, self.chain_port)))

            try:
                while True:
                    r, w, x = select.select([self.request, chan], [], [])
                    if self.request in r:
                        data = self.request.recv(1024)
                        if len(data) == 0:
                            break
                        chan.send(data)
                    if chan in r:
                        data = chan.recv(1024)
                        if len(data) == 0:
                            break
                        self.request.send(data)

                chan.close()
                self.request.close()

                peer_name = self.request.getpeername()
                print('Forward Tunnel closed from %r' % (peer_name,))
            except Exception as e:
                print repr(e)
                pass

        def finish(self):
            print("Handler Finished")

    @staticmethod
    def reverse_handler(chan, host, port):
        import socket
        import select

        sock = socket.socket()
        try:
            sock.connect((host, port))
        except Exception as e:
            print('Forwarding request to %s:%d failed: %r' % (host, port, e))
            return

        print('Connected!  Tunnel open %r -> %r -> %r' % (chan.origin_addr,
                                                          chan.getpeername(), (host, port)))

        try:
            while True:
                r, w, x = select.select([sock, chan], [], [])
                if sock in r:
                    data = sock.recv(1024)
                    if len(data) == 0:
                        break
                    chan.send(data)
                if chan in r:
                    data = chan.recv(1024)
                    if len(data) == 0:
                        break
                    sock.send(data)
            chan.close()
            sock.close()
            print('Reverse Tunnel closed from %r' % (chan.origin_addr,))
        except Exception as e:
            print(repr(e))

    class ServerWorker(threading.Thread):
        """ServerWorker"""

        def __init__(self, context, parent, service):
            threading.Thread.__init__(self)
            self.context = context
            self.poller = zmq.Poller()
            self.first = False
            self.parent = parent
            self.service = service

        def activity(self, socket, timeout=10):
            return socket in dict(self.poller.poll(timeout))

        def run(self):
            worker = self.context.socket(zmq.DEALER)
            worker.connect('inproc://backend')

            self.poller.register(worker, zmq.POLLIN)

            pinned_task = None

            while True:
                if self.activity(worker):
                    if self.first is False:
                        next_worker = RemoteService.ServerWorker(self.context, self.parent, self.service)
                        next_worker.daemon = True
                        self.parent.workers.append(next_worker)
                        print("Total workers = ", len(self.parent.workers))
                        self.first = True

                    # deal with identity
                    ident, msg = worker.recv_multipart()
                    print ("identity: ", ident)
                    print("got message =" + str(msg))

                    result = json.loads(msg)

                    if result["command"] == "__keep_alive__":
                        print "returning...", worker
                        #worker.send_multi("", zmq.SNDMORE)
                        worker.send_multipart([ident, msg])
                        continue

                    if result["command"] == "__service__":
                        # find if the service already exists and set it to running state
                        # otherwise start it in running state
                        match = False
                        for task in self.service.task_list:
                            if task.name() == result["output"]["name"]:
                                match = True
                                task.set_running_status(worker, self.service)
                                self.service.running_task(task)
                                pinned_task = task
                                pinned_task.identity = ident
                                pinned_task.send_ctrl("__success__", ident, dict())

                        # create a new running service
                        if match is False:
                            running_task = RunningTask(result["output"], worker, self.service)
                            self.service.add_services(running_task)
                            self.service.running_task(running_task)
                            pinned_task = running_task
                            pinned_task.identity = ident
                            pinned_task.send_ctrl("__success__", ident, dict())
                        continue

                    if pinned_task is not None:
                        if pinned_task.worker is None:
                            pinned_task.set_running_status(worker, self.service)
                            pinned_task.identity = ident

                        pinned_task.handle_recv(result)

            worker.close()

        def send_ctrl(self, worker, tag, uid, msg):
            response = dict()
            response["command"] = tag
            response["output"] = msg
            response["uid"] = uid
            print("sending: ", json.dumps(response))
            worker.send_multipart(json.dumps(response))

    class ServerTask(threading.Thread):
        """ServerTask"""

        def __init__(self, port, parent):
            threading.Thread.__init__(self)
            self.workers = []
            self.parent = parent
            self.port = port

        def run(self):
            context = zmq.Context()
            frontend = context.socket(zmq.ROUTER)
            frontend.bind('tcp://*:' + str(self.port))

            backend = context.socket(zmq.DEALER)
            backend.bind('inproc://backend')

            self.workers = []
            worker = RemoteService.ServerWorker(context, self, self.parent)
            worker.daemon = True
            worker.start()
            self.workers.append(worker)

            zmq.proxy(frontend, backend)

            frontend.close()
            backend.close()
            context.term()

    def __init__(self):
        self.address = None
        self.port = None
        self.stream = None
        self.keep_alive_timer = 0
        self.task_list = []
        self.root_dir = None
        self.owned_tasks = []
        self.active_connections = dict()
        self.reverse_tunnel_map = dict()
        self.tempfile = ""
        self.zmq_master_conn = None
        self.timeout = 10

    @staticmethod
    def get_free_local_port():
        """
        :return:
        """
        import socket
        s = socket.socket()
        s.bind(("", 0))
        local_port = s.getsockname()[1]
        s.close()
        return local_port

    @staticmethod
    def check_pid(path):
        """ Check For the existence of a unix pid. """
        import glob
        result = glob.glob(path + "/active_service_*")

        for res in result:
            m = os.path.basename(res)
            m = m.split("_")
            print(m)
            pid = int(m[-1])

            try:
                print("checking pid:" + str(pid))
                os.kill(pid, 0)  # TODO: make sure windows version works for this...
            except OSError:
                # pid doesn't exist remove it...
                if os.path.exists(res):
                    print("removing file: " + res)
                    os.remove(res)

    def initialize(self):
        if len(sys.argv) < 4:
            print("Usage: remote_service.py <camera-root-dir> <read-port> <address> [timeout]")
            sys.exit(0)

        self.root_dir = sys.argv[1]
        self.port = sys.argv[2]
        self.address = sys.argv[3]

        if len(sys.argv) >= 5:
            try:
                self.timeout = int(sys.argv[4])
            except:
                self.timeout = 10

        path = os.path.expanduser("~/.camera/")
        if not os.path.exists(path):
            os.makedirs(path)

        ctx = zmq.Context()

        master_port = self.get_free_local_port()
        self.zmq_master_conn = RemoteService.ServerTask(master_port, self)
        self.zmq_master_conn.daemon = True
        self.zmq_master_conn.start()

        RemoteService.check_pid(path)

        self.tempfile = path + "/" + "active_service_" + str(master_port) + "_" + str(os.getpid())
        filename = open(self.tempfile, "w")
        filename.write("master_port = " + str(master_port) + "\n")
        filename.write("master_address = " + str(self.address) + "\n")
        filename.write("pid = " + str(os.getpid()) + "\n")
        filename.close()

        s = ctx.socket(zmq.PAIR)
        # if str(self.port).find(":") > 0:
        #    s.connect('tcp://' + str(self.port))
        # else:
        #    s.connect('tcp://localhost:' + str(self.port))

        print "tcp://*:" + str(self.port)
        s.bind("tcp://*:" + str(self.port))

        self.stream = ZMQStream(s)
        self.stream.on_recv_stream(self.on_recv_data)

        beats = ioloop.PeriodicCallback(self.keep_alive, 500)
        beats.start()

        # services = os.path.dirname(__file__) + "/services.json"
        local_services = rs.root_dir + "/services.json"

        # load external libraries
        def load_service(path):
            service_file = open(path)
            data = service_file.readlines()
            data = "".join(data)
            service_data = json.loads(data)
            service_file.close()

            # if single dictionary instead of array
            # mimic an array object
            if isinstance(service_data, dict):
                service_data = [service_data]

            for service in service_data:
                if service["type"] == "import":
                    res = self.launch_import_service(service)
                elif service["type"] == "direct":
                    # start the service in a thread and create ZMQ socket to communicate with it.
                    res = self.launch_direct_service(service)

                if res is False:
                    print("Service ", service["name"], " failed to launch")

        # TODO: get this path from environment?
        # These services are local to the machine

        # if os.path.exists(services):
        #    load_service(services)

        if os.path.exists(local_services):
            load_service(local_services)

        for root, dir, files in os.walk(rs.root_dir):
            for items in fnmatch.filter(files, "services.json"):
                result = os.path.join(root, items)
                load_service(result)

    def launch_import_service(self, service):
        def create_task_service(module, class_name):
            try:
                my_class = getattr(importlib.import_module(module, class_name), class_name)
                instance = my_class
                return instance
            except:
                return False

        task = create_task_service(str(service["source"]).replace(".py", ""),
                                   service["name"])

        if task is None or not issubclass(task, Task):
            print("Service: ", service["name"], " failed to load")
            return False

        print("Loading Direct Task: ", service["name"])
        self.register_service(task())
        return True

    def launch_direct_service(self, service):
        print("Loading Service Task: ", service["name"])
        self.register_service(RunningTask(service))
        return True

    def start(self):
        ioloop.IOLoop.instance().start()

    def shutdown(self):
        ioloop.IOLoop.instance().stop()

    def running_task(self, service):
        task_descriptions = []
        task_description = dict()
        task_description["name"] = service.name()
        task_descriptions.append(task_description)

        self.send_data(-1, "__running_services__", task_descriptions)

    def add_services(self, service):
        self.register_service(service)

        task_descriptions = []
        task_description = dict()
        task_description["name"] = service.name()
        task_description["inputs"] = service.input_vars()
        task_description["outputs"] = service.output_vars()
        task_descriptions.append(task_description)

        self.send_data(-1, "__add_services__", task_descriptions)

    def remove_services(self, service):
        task_descriptions = []
        task_description = dict()
        task_description["name"] = service.name()
        task_description["inputs"] = service.input_vars()
        task_description["outputs"] = service.output_vars()
        task_descriptions.append(task_description)

        self.send_data(-1, "__remove_services__", task_descriptions)

    def send_all_services(self):
        task_descriptions = []
        for task in self.task_list:
            task_description = dict()
            task_description["name"] = task.name()
            task_description["inputs"] = task.input_vars()
            task_description["outputs"] = task.output_vars()
            task_descriptions.append(task_description)

        self.send_data(-1, "__all_services__", task_descriptions)

    def on_recv_data(self, stream, message):
        self.keep_alive_timer = 0
        byte_data = message[0]

        try:
            data = json.loads(byte_data)
            if data["command"] == "__respond_keep_alive__":
                return
            self.recv_data(data)
        except:
            self.send_data(-1, "error", "unable to parse" + str(byte_data))

    def handle_tasks(self, uid, selected_tasks):

        """
        for items in selected_tasks:
            for task in self.task_list:
                if items["name"] == task.name():
                    ldata = dict()
                    ldata["output"] = ""
                    ldata["input_port"] = items["input_port"]
                    ldata["output_port"] = items["output_port"]

                    import threading
                    robject = RemoteService.RunObject(task, self, uid)

                    t = threading.Thread(target=robject.initialize_func, args=(ldata,))
                    t.daemon = True
                    t.start()
                    self.owned_tasks.append(robject)
        """

        for items in selected_tasks:
            for task in self.task_list:
                if items["name"] == task.name():
                    # execute only if not started or running already
                    if task.state == "Initialized":
                        task.execute()  # start the service so it can connect back...
                    self.owned_tasks.append(task)

        return "Tasks launched.."

    def run_tasks(self, uid):
        for f in self.owned_tasks:
            f.uid = uid
            ret = f.run_func()
            self.send_data(uid, "response", ret)
        return "Tasks started.."

    def connect_to(self, data):
        # TODO: replace with NODE
        address = data["address"]
        username = data["username"]
        password = data["password"]
        port = data["port"]

        if address in self.active_connections:
            return "address :" + address + " already exists"

        import paramiko
        client = paramiko.SSHClient()
        client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        client.load_system_host_keys()
        # print "connecting: ", address, int(port), username, password

        client.connect(address, int(port), username, password)
        self.active_connections[address] = client
        return "address :" + address + " added to connections"

    def forward_tunnel(self, data):
        address = data["address"]
        source_port = data["source_port"]
        dest_host = data["dest_host"]
        dest_port = data["dest_port"]

        if address not in self.active_connections:
            raise Exception("Active connection not found for " + address)

        _transport = self.active_connections[address].get_transport()

        def _forward_tunnel(local_port, remote_host, remote_port, transport):

            class SubHandler(RemoteService.Handler):
                chain_host = remote_host
                chain_port = int(remote_port)
                ssh_transport = transport

            fs = RemoteService.ForwardServer(('', int(local_port)), SubHandler)

            import threading
            server_thread = threading.Thread(target=fs.serve_forever)
            server_thread.daemon = True
            server_thread.start()
        _forward_tunnel(source_port, dest_host, dest_port, _transport)
        return "OK"

    def reverse_tunnel(self, data):
        _address = data["address"]
        _remote_port = int(data["remote_port"])
        _remote_host = data["remote_host"]
        _server_port = int(data["server_port"])

        if _address not in self.active_connections:
            raise Exception("Active connection not found for " + _address)

        _transport = self.active_connections[_address].get_transport()

        class ReverseHandler:
            def __init__(self):
                self.ports = dict()

            def my_handler(self, channel, src_addr, dest_addr_port):
                import threading
                src_port = dest_addr_port[1]
                if src_port in self.ports:
                    dest_host = self.ports[src_port][0]
                    dest_port = self.ports[src_port][1]
                    thr = threading.Thread(target=RemoteService.reverse_handler,
                                           args=(channel, dest_host, dest_port))
                    thr.setDaemon(True)
                    thr.start()

        if _address not in self.reverse_tunnel_map:
            self.reverse_tunnel_map[_address] = dict()
            self.reverse_tunnel_map[_address]["handler"] = ReverseHandler()
        self.reverse_tunnel_map[_address]["handler"].ports[_server_port] = [_remote_host, _remote_port]

        _transport.request_port_forward('', _server_port, self.reverse_tunnel_map[_address]["handler"].my_handler)
        return "OK"

    def recv_data(self, data):
        try:
            command = data["command"]
            uid = data["uid"]

            # execute task list
            if command == "__setup_task_list__":
                data = data["output"]
                ret = self.handle_tasks(uid, data)
                self.send_data(uid, "response", ret)
                return

            if command == "__run_task_list__":
                data = data["output"]
                ret = self.run_tasks(uid)
                self.send_data(uid, "response", ret)
                return

            if command == "__connect__":
                data = data["output"]
                ret = self.connect_to(data)
                self.send_data(uid, "response", ret)
                return

            if command == "__forward_tunnel__":
                data = data["output"]
                ret = self.forward_tunnel(data)
                self.send_data(uid, "response", ret)
                return

            if command == "__reverse_tunnel__":
                data = data["output"]
                ret = self.reverse_tunnel(data)
                self.send_data(uid, "response", ret)
                return

            if command == "__task_command__":
                data = data["output"]
                self.process_task_command(uid, data)
                #self.send_data(uid, "response", ret)
                return

            found_match = False
            for task in self.task_list:
                if task.name() == command:
                    found_match = True

                    import threading
                    run_object = RemoteService.RunObject(task, self, uid)
                    t = threading.Thread(target=run_object.run_func, args=(data["output"],))
                    t.daemon = True
                    t.start()
                    self.owned_tasks.append(run_object)

            if not found_match:
                self.send_data(uid, "error", "No task found named: " + command)

        except Exception as e:
            print(repr(e))
            msg = "unable to parse: " + "error = " + repr(e) + ",\n" + str(data)
            self.send_data(-1, "error", msg)

    def process_task_command(self, uid, data):
        input_task = data["task"]
        print ("Looking for", input_task)
        for task in self.task_list:
            if input_task == task.name():
                print ("Found: ", task.name())
                if task.state is not "Running":
                    return dict(task=input_task, comm="Task " + input_task + " is currently not running")
                return task.process(uid, data)
        return "No task found"

    def send_data(self, uid, tag, msg):
        response = dict()
        response["command"] = tag
        response["output"] = msg
        response["uid"] = uid
        self.stream.send(json.dumps(response))

    def keep_alive(self):
        self.send_data(-1, "__keep_alive__", "")
        if self.keep_alive_timer >= self.timeout:
            print("shutting down")

            if os.path.exists(self.tempfile):
                print("removing active file: ", self.tempfile)
                os.remove(self.tempfile)

            for worker in self.task_list:
                print("terminating worker: ", worker)
                worker.terminate()

            # tell all running tasks to terminate
            try:
                for f in self.owned_tasks:
                    f.terminate_func()
            except:
                pass

            self.shutdown()

        self.keep_alive_timer += 1

    def register_service(self, task):
        self.task_list.append(task)


if __name__ == "__main__":
    rs = RemoteService()
    rs.initialize()
    rs.send_all_services()
    rs.start()

    # ensure every subprocess gets removed..
    pgrp = os.getpgid(0)
    os.killpg(pgrp, signal.SIGKILL)


