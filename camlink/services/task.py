class Task:
    def __init__(self, name, machine, service):
        self.name = name
        self.machine = machine
        self.output_machines = []
        self.output_tasks = []
        self.input_ports = []  # for now one input multiple outputs
        self.output_ports = []
        self.service = service
        self.is_ready = False
        self.result = None
        self.has_x11 = False
        self.no_comm = False

    def __repr__(self):
        return "task: " + self.name

    def send_to(self, task, machine=None):
        self.output_machines.append(machine)
        self.output_tasks.append(task)

    def communicate(self, comm):

        if self.no_comm:
            return None

        if self.result is not None:
            # raise Exception("Already Waiting for event")
            return None

        self.service.communicate_with_task(self.name, comm)

        # TODO: Handle failure modes...
        import time
        while self.result is None:
            time.sleep(1)

        ret = self.result
        self.result = None

        return ret

    def set_result(self, result):
        self.result = result

    def get_free_port(self):
        command = [{"type": "request_free_port"}]
        return self.service.node.get_free_remote_port()

        return result

    def request_meta_data(self):
        result = self.communicate("config_data")
        return result

    def publish(self, task, ip, frequency, tunnel, known_port=None):

        if known_port is None:
            send_network = [{"type": "publish", "frequency": frequency}]
        else:
            send_network = [{"type": "publish", "frequency": frequency, "port": known_port}]
        # send_network = [{"type": "publish", "freq": frequency}]

        result = self.communicate(send_network)

        self.output_ports.append(["publish", task, ip, frequency, result[0]])

        if tunnel:
            local_port = self.service.node.get_free_local_port()
            self.service.node.forward_tunnel(local_port, "localhost", int(result[0]))
            # task.service.node.forward_tunnel(int(result[0]), "localhost", local_port)
            print ("forward tunnel: ", int(local_port), int(result[0]))

            ip = "localhost"
            result[0] = local_port

        recv_network = [{"type": "subscribe", "ip": ip, "port": result[0]}]
        task.communicate(recv_network)

        task.input_ports.append(["subscribe", self, ip, result[0]])
        return result[0]

    def push(self, task, ip, frequency, tunnel, known_port=None):

        if known_port is None:
            send_network = [{"type": "push", "frequency": frequency}]
        else:
            send_network = [{"type": "push", "frequency": frequency, "port": known_port}]

        result = self.communicate(send_network)

        self.output_ports.append(["push", task, ip, frequency, result[0]])

        if tunnel:
            local_port = self.service.node.get_free_local_port()
            self.service.node.forward_tunnel(local_port, "localhost", result[0])
            print ("forward tunnel: ", local_port, result[0])

            result[0] = local_port
            ip = "localhost"

        recv_network = [{"type": "pull", "ip": ip, "port": result[0]}]
        task.communicate(recv_network)

        task.input_ports.append(["pull", self, ip, result[0]])
        return result[0]

    def pull(self, task, ip, tunnel, known_port=None):

        if known_port is None:
            send_network = [{"type": "push", "frequency": frequency}]
        else:
            send_network = [{"type": "push", "frequency": frequency, "port": known_port}]

        result = self.communicate(send_network)

        self.output_ports.append(["push", task, ip, frequency, result[0]])

        if tunnel:
            local_port = self.service.node.get_free_local_port()
            self.service.node.forward_tunnel(local_port, "localhost", result[0])
            print ("forward tunnel: ", local_port, result[0])

            result[0] = local_port
            ip = "localhost"

        recv_network = [{"type": "pull", "ip": ip, "port": result[0]}]
        task.communicate(recv_network)

        task.input_ports.append(["pull", self, ip, result[0]])
        return result[0]



    def reconnect(self):

        print("Reconnecting Task: ", self.name)

        for input_port in self.input_ports:
            recv_network = [{"type": input_port[0], "ip": input_port[2], "port": input_port[3]}]
            print self, recv_network
            #self.communicate(recv_network)
            self.service.communicate_with_task(self.name, recv_network)

        for output_port in self.output_ports:
            recv_network = [{"type": output_port[0], "frequency": input_port[2], "port": input_port[3]}]
            print self, recv_network
            #self.communicate(recv_network)
            self.service.communicate_with_task(self.name, recv_network)

        self.service.communicate_with_task(self.name, "start")
