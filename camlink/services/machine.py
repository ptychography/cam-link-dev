from .service import *
from .task import *


class Machine:
    def __init__(self, name, node=None, links=None):
        self.name = name
        self.service = None
        self.has_environment = False
        self.tasks = []
        self.has_initialized = False
        self.task_ready = None

        self.node = node
        self.links = links
        self.output_messages = False
        self.has_x11 = False

    def __repr__(self):
        return "machine: " + self.name

    def __getitem__(self, item):
        return self.get_task(item)

    def add(self, task):
        self.tasks.append(task)
        return self

    def get_task(self, name):
        for task in self.tasks:
            if task.name == name:
                return task
        return None

    def set_service(self, node, links):
        print("creating service for ", node.name)
        self.service = Service(node, links)
        self.service.set_data_callback(self.data_callback)
        self.has_environment = self.service.has_environment()

        if not self.has_environment:
            print("Setting environment for", self.name)
            self.has_environment = self.service.create_environment()

        if not self.has_environment:
            print("Environment failed to setup..")
            return False

        return True

    def data_callback(self, data_id, response):
        print "DATA returned ", str(response)
        if data_id == self.task_ready:
            print("machine tasks are ready and initialized")

        if isinstance(response, dict) and "task" in response and "comm" in response:
            for task in self.tasks:
                if task.name == response["task"]:
                    task.set_result(response["comm"])

    def service_loaded(self, services, state):
        print str(services)
        for service in services:
            print("updated machine", self.name, ":", service["name"], " state = ", state)
            if state == "running":
                for task in self.tasks:
                    if task.name == service["name"]:
                        if task.is_ready:
                            task.reconnect()
                        else:
                            task.is_ready = True

        self.has_initialized = True

    def toggle_messages(self):
        self.output_messages = not self.output_messages

    def output_callback(self, message):

        if self.output_messages is True:
            message = message.strip()
            print (self.name, message)

    def all_services(self):
        return self.service.services

    def initialize(self):
        if self.node is None:
            return False

        return self.set_service(self.node, self.links)

    def setup(self):

        if not self.has_environment:
            return False

        has_x11 = self.has_x11

        if not has_x11:  # check each task
            for task in self.tasks:
                if task.has_x11:
                    has_x11 = True
                    break

        return self.service.setup(self.service_loaded, self.output_callback, has_x11)

    def start(self):
        if not self.has_initialized:
            return False

        # send tasks to service
        result = [dict(name=x.name) for x in self.tasks]
        self.task_ready = self.service.setup_task_list(result)

        import time
        if len(self.tasks) > 0:
            while True:
                match = True
                for task in self.tasks:
                    if task.is_ready is False:
                        match = False
                        break

                if match is True:
                    break
                time.sleep(1)
        return True

    def run(self):
        self.service.run_task_list()

    def install_default_applications(self, apps):
        for app in apps:
            res = self.service.install_app(app)
            if not res:
                return False
        return True

    def configure_tasks(self, tasks):
        try:
            len(tasks)  # test if the task is an array
        except:
            tasks = [tasks]

        services = self.all_services()

        new_tasks = []
        for service in services.keys():
            for local_task in tasks:
                if service == local_task:
                    # make sure task is not already in list
                    gt = Task(local_task, self, self.service)
                    new_tasks.append(gt)

        before_add = len(self.tasks)

        for new_task in new_tasks:
            match = False
            for task in self.tasks:
                if task.name == new_task.name:
                    match = True
                    break

            if not match:
                self.tasks.append(new_task)

        return len(self.tasks) != before_add

