import os
import json
import zmq
from zmq.eventloop.zmqstream import ZMQStream

from plugins.task_interface import Task
import threading


class Service:

    app_dirname = os.path.dirname(__file__) + "/../../"

    class ServiceTask(Task):

        def __init__(self, entry):
            self.entry = entry

        def name(self):
            return self.entry["name"]

        def input_vars(self):
            return self.entry["inputs"]

        def output_vars(self):
            return self.entry["outputs"]

    def __init__(self, node, links=None):
        self.node = node
        self.links = links
        self.local_port = None
        self.remote_port = None

        self.root_service = self.node.config_env
        self.remote_service = self.node.config_env + "/cam-link/plugins/remote_service.py"
        self.python_command = self.node.config_env + "/conda/bin/python"
        self.stream = None
        self.services = dict()
        self.service_callback = None

        self.unique_counter = 0
        self.data_callback = None

    def has_environment(self):
        # TODO: better check for node state
        if len(self.node.config_env) == 0 or not self.node.path_exists(self.node.config_env):
            # print(self.node.name, " environment not setup to start service")
            return False

        if not self.node.path_exists(self.remote_service) or not self.node.path_exists(self.python_command):
            # print(self.node.name, " environment not setup to start service: " + self.remote_service)
            return False

        return True

    def create_environment(self):

        # return True

        # environment already exists
        if self.has_environment():
            return True

        if len(self.node.config_env) == 0:
            print("No configuration environment set")
            return False

        print("Creating environment for", self.node.name, "at", self.node.config_env)
        # create new environment

        if not self.node.path_exists(self.node.config_env):
            self.node.mkdir(self.node.config_env)

        if not self.node.path_exists(self.node.config_env):
            print("Could not create remote CAMERA environment")
            return False

        # check if Python executable exists
        # this assumes that installation is available
        if not self.node.path_exists(self.python_command):
            filename = Service.app_dirname + "/config/build_camera_env.sh"
            # copy file

            print("Putting", filename, self.node.config_env)
            self.node.put(filename, self.node.config_env)

            command = "/bin/bash " + self.node.config_env + "/build_camera_env.sh " + self.node.config_env
            print("Creating environment ", command)
            result = self.node.iexecute(command)

            result = result.split("__")

            if len(result) < 2:
                print("Environment creation failed...")
                return False

            # script returns three results, 0 is dest directory
            if self.node.config_env != result[-2].strip():
                print("Destination directory creation failed",
                      result[-2].strip(), self.node.config_env)
                return False

            if self.python_command != result[-1].strip():
                print("Failed to find Python in remote installation",
                      result[-1].strip())
                return False

        # test if remote service already exists
        # if not then install it..
        if not self.node.path_exists(self.remote_service):
            print "Installing remote service.."
            plugin_dir = Service.app_dirname + "/plugins"
            dest_dir = self.node.config_env + "/cam-link/plugins"
            self.node.put_dir(plugin_dir, dest_dir)

        return True

    def install_app(self, app):

        if "name" not in app:
            return False

        app_path = app["name"]
        environment = None
        template = None

        if "environment" in app:
            environment = app["environment"]

        if "template" in app:
            template = app["template"]

        # print "TEMPLATE", template
        # print "ENV", environment

        # copy the path and install dependencies on remote machine
        dest_dir = self.node.config_env + "/cam-link/apps/" + app_path

        #if self.node.path_exists(dest_dir):
        #    print("Application", app_path, "already exists, Skipping")
        #    return True

        app_dir = Service.app_dirname + "/apps/" + app_path

        service = app_dir + "/services.json"
        if not os.path.exists(service):
            return False

        service_data = open(service, "r")
        meta_data = service_data.readlines()

        meta_data = "\n".join([f.strip() for f in meta_data])
        meta_data = json.loads(meta_data)

        if not isinstance(meta_data, dict):
            return False

        if environment is not None:
            try:
                meta_data.update({"environment": environment})
            except:
                pass

        print meta_data

        if "deps" in meta_data:
            pip_deps = []
            conda_deps = []

            # only supports pip and conda
            for dep in meta_data["deps"]:
                if ":" in dep:
                    key = dep.split(":")
                    if key[0] == "pip":
                        pip_deps.append(key[1])
                    else:
                        conda_deps.append(key[1])
                else:
                    conda_deps.append(dep)

            if len(pip_deps) > 0:
                deps = " ".join(pip_deps)
                command = self.node.config_env + "/conda/bin/pip install " + deps
                print("Installing pip dependency command:", command)
                self.node.iexecute(command)

            if len(conda_deps) > 0:
                deps = " ".join(conda_deps)
                command = self.node.config_env + "/conda/bin/conda install -q -y " + deps
                print("Installing conda dependency command:", command)
                self.node.iexecute(command)

        print ("Installing application from ", app_dir, "to", dest_dir)
        self.node.put_dir(app_dir, dest_dir)

        exec_path = meta_data["executable"]

        root_bin_path = self.node.config_env + "/conda"
        if template is not None and "ROOT_BIN_PATH" in template:
            root_bin_path = template["ROOT_BIN_PATH"]

        exec_path = exec_path.replace("@ROOT_BIN_PATH@", root_bin_path)

        app_path_loc = self.node.config_env + "/cam-link/apps/" + app_path
        if template is not None and "APP_PATH" in template:
            app_path_loc = template["APP_PATH"]

        if template is not None:
            for names in template:
                if "@" + names + "@" in exec_path:
                    exec_path = exec_path.replace("@" + names + "@", template[names])

        meta_data["executable"] = exec_path.replace("@APP_PATH@", app_path_loc)

        meta_data_string = json.dumps(meta_data)
        print "writing to file"
        self.node.write_to_file(meta_data_string, dest_dir + "/services.json")
        print "done..."

        return True

    def initialize_service(self, tasks):
        for task in tasks:
            s_task = Service.ServiceTask(task)
            self.services[s_task.name()] = s_task

        self.service_callback(tasks, "initialized")

        if self.links is not None:
            for link in self.links:
                link.set_service_link(self)

    def add_services(self, tasks):
        for task in tasks:
            s_task = Service.ServiceTask(task)
            self.services[s_task.name()] = s_task

        self.service_callback(tasks, "added")

    def remove_services(self, tasks):
        for task in tasks:
            s_task = Service.ServiceTask(task)
            if s_task.name() in self.services:
                del self.services[s_task.name()]

        self.service_callback(tasks, "removed")

    def is_running(self, tasks):
        self.service_callback(tasks, "running")

    def on_recv_data(self, stream, msg):
        byte_data = msg[0]
        try:
            data = json.loads(byte_data)
            if data["command"] == "__keep_alive__":
                self.send_data("__respond_keep_alive__", "")
            elif data["command"] == "__all_services__":
                tasks = data["output"]
                self.initialize_service(tasks)
            elif data["command"] == "__add_services__":
                tasks = data["output"]
                self.add_services(tasks)
            elif data["command"] == "__remove_services__":
                tasks = data["output"]
                self.remove_services(tasks)
            elif data["command"] == "__running_services__":
                tasks = data["output"]
                self.is_running(tasks)
            elif data["command"] == "response":
                uid = data["uid"]
                print(str(uid) + " " + str(data))
                if self.data_callback is not None:
                    self.data_callback(uid, data["output"])
        except:
            print("unable to parse response: ", str(byte_data))

    def send_data(self, tag, msg):
        response = dict()
        response["command"] = tag
        response["output"] = msg
        response["uid"] = self.get_unique_id()

        self.stream.send(json.dumps(response))
        return response["uid"]

    def set_data_callback(self, cb):
        self.data_callback = cb

    def stream_thread(self):
        while True:
            msg = self.stream.recv()
            self.on_recv_data(None, [msg])

    def setup(self, service_callback, output_callback, with_x11=False):
        """
        connect to remote machine
        :return:
        """
        import camlink.comm.machine_profile as machine_profile

        if not self.has_environment():
            return False

        self.service_callback = service_callback
        self.local_port = self.node.get_free_local_port()
        self.remote_port = int(self.node.get_free_remote_port()[1])

        print("starting ZMQ server ", self.local_port, self.remote_port)

        if self.node.mp["name"] != "localhost":

            # ret = self.node.reverse_forward_tunnel(self.remote_port, "localhost", self.local_port)
            ret = self.node.forward_tunnel(self.local_port, "localhost", self.remote_port)

            if ret is not None:
                self.remote_port = ret
        else:
            self.remote_port = self.local_port

        ctx = zmq.Context()
        s = ctx.socket(zmq.PAIR)
        s.connect("tcp://localhost:%s" % str(self.local_port))

        self.stream = s
        # self.stream = ZMQStream(s)
        # self.stream.on_recv_stream(self.on_recv_data)

        thr = threading.Thread(target=self.stream_thread)
        thr.setDaemon(True)
        thr.start()

        remote_string = str(self.remote_port)

        # if self.node.mp["use_ncat"]:
        #    remote_string = machine_profile.global_mp[self.node.mp["parent"]]["connection"].address + ":" + str(self.remote_port)

        # command_prefix = "source " + self.node.config_env + "/conda/bin/activate && "
        # command_prefix += "export PYTHONUSERBASE=/tmp && "
        command_prefix = ""
        # ignore site-packages and environment variables...
        command = command_prefix + self.python_command + " -s -E " + self.remote_service + " "
        command += self.root_service + " " + str(remote_string) + " " + self.node.address
        command += " 30 dummy_argument"  # dummy argument

        print("remote execution: ", self.node.name, command)

        try:
            if with_x11:
                self.node.bg_x11_execute(command, output_callback)
            else:
                self.node.bg_execute(command, output_callback)
        except Exception as e:
            print("Service execution for ", self.node.name, "failed: ", repr(e))
            return False

        print "ZMQ service started..."
        return True

    def install(self, package):
        return self.send_data("Install", {"package": package})

    def remote_execute(self, cmd):
        return self.send_data("Execute", {"command": cmd})

    def run_task(self, name, input_params):
        return self.send_data(name, input_params)

    def setup_task_list(self, tasks):
        return self.send_data("__setup_task_list__", tasks)

    def run_task_list(self):
        return self.send_data("__run_task_list__", "")

    def connect_to(self, address, username, password, port=22):
        if callable(password):
            password = password()
        info = dict(username=username, password=password, address=address, port=port)
        return self.send_data("__connect__", info)

    def forward_tunnel(self, address, source_port, dest_host, dest_port):
        info = dict(address=address, source_port=source_port, dest_host=dest_host, dest_port=dest_port)
        return self.send_data("__forward_tunnel__", info)

    def reverse_tunnel(self, address, server_port, remote_host, remote_port):
        info = dict(address=address, server_port=server_port, remote_host=remote_host, remote_port=remote_port)
        return self.send_data("__reverse_tunnel__", info)

    def get_unique_id(self):
        self.unique_counter += 1
        return self.unique_counter

    def get_task_names(self):
        return self.services.keys()

    def get_tasks(self):
        return self.services

    def get_task_params(self, name):
        return self.services[name]

    def communicate_with_task(self, task, comm):
        info = dict(task=task, comm=comm)
        return self.send_data("__task_command__", info)
