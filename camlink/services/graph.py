from .machine import *
from .task import *
from camlink.comm import graph as comm_graph
from camlink.comm.machine_profile import global_mp
from zmq.eventloop import ioloop
import json


class Graph:
    def __init__(self):
        self.connection_graph = None
        self.task_graph = None
        self.network_graph = None
        self.services = []
        self.machines = []
        self.graph_initialized_callback = None
        self.connect_tasks = None
        self.connection_graph = comm_graph.Graph()

    def get_machine(self, name):
        for mach in self.machines:
            if mach.name == name:
                return mach
        return None

    def get_machines(self):
        return self.machines

    def __getitem__(self, item):
        return self.get_machine(item)

    def get_task(self, machine_name, task_name):
        for mach in self.machines:
            if mach.name == machine_name:
                for task in mach.tasks:
                    if task.name == task_name:
                        return task

        return None

    def add(self, mach):
        self.machines.append(mach)

    def start_services(self, machine_list):
        """
        Start services on remote machines
        :param machine_list: list of machines to start and names
        :param install_list
        :return: True or false if successful
        """

        for machine in machine_list:
            apps = None
            if "apps" in machine:
                apps = machine["apps"]
            res = self.start_service(machine["machine"], apps)

            if not res:
                return False
        return True

    def start_service(self, machine, install_list=None):
        """
        Start services on remote machines
        :param machine: list of machines to start and names
        :param install_list
        :return: True or false if successful
        """

        # Create SSH connections to machines
        result = self.connection_graph.start_connections([machine])

        if result is False:
            print("Machine failed to connect.")
            return False

        graph_machine = None
        match = False

        # Now start services on these Machines
        for node in self.connection_graph.nodes:
            if node.name == machine:
                graph_machine = Machine(machine, node)
                self.machines.append(graph_machine)
                match = True
                break

        if not match:
            print("Machine", machine, "failed to start. Returning...")
            return False

        # TODO: fail if not all services are present?
        # initialize each connection
        result = graph_machine.initialize()

        if not result:
            print("Machine", machine.name, " failed to initialize")
            return False

        if install_list is not None:
            res = graph_machine.install_default_applications(install_list)
            if res:
                print("Installed: ", install_list)
            else:
                print("Failed to install: ", install_list, "for machine", machine)
                return False
        return True

    def setup(self):
        print("Setting up ")
        #try:
        #    import threading
        #    t = threading.Thread(target=ioloop.IOLoop.instance().start)
        #    t.daemon = True
        #    t.start()
        #except:
        #    pass

        # setup services
        for graph_machine in self.machines:
            result = graph_machine.setup()
            if not result:
                print("Machine", graph_machine.name, " failed to setup...")
                return False

        def is_initialized():
            for machine in self.machines:
                print "checking machines...", machine, machine.has_initialized
                if machine.has_initialized is False:
                    return False

            # beats.stop()
            return True

        # beats = ioloop.PeriodicCallback(is_initialized, 100)
        # beats.start()

        # try:
        #    import threading
        #    t = threading.Thread(target=ioloop.IOLoop.instance().start)
        #    t.daemon = True
        #    t.start()
        # except:
        #    pass

        import time
        while True:
            if is_initialized():
                break
            time.sleep(2)

        print("Service for", self.machines, " started...")
        return True

    def start_tasks(self):
        for m in self.machines:
            for t in m.tasks:
                print(m.name, "will run", t.name)
                m.start()

        return True

    def parse(self, filename):
        #try:
        #    import threading
        #    t = threading.Thread(target=ioloop.IOLoop.instance().start)
        #    t.daemon = True
        #    t.start()
        #except:
        #    pass

        filename = os.path.abspath(os.path.expanduser(filename))
        f = open(filename, "r")
        str_contents = f.read()
        return self.parse_stream(str_contents)

    def parse_stream(self, str_contents):
        try:
            contents = json.loads(str_contents)
            if "params" in contents:
                params = contents["params"]

                for param in params.keys():
                    print "@" + param + "@", params[param]
                    str_contents = str_contents.replace("@" + param + "@", params[param])
                contents = json.loads(str_contents)

            if "graph" not in contents:
                print("No graph object to execute")
                return False

            # if "configure" not in contents["graph"] or "connectivity" not in contents["graph"]:
            #    print("No configure or connectivity in graph")
            #    return False

            if "configure" not in contents["graph"]:
                print("No configure or connectivity in graph")
                return False

            if "machine_access_file" in contents:
                global_mp.read_machine_list(contents["machine_access_file"])

            if "machines" in contents:
                for m in contents["machines"]:
                    global_mp.parse_json_entry(m)

            graph = contents["graph"]
            configure_tasks = graph["configure"]

            for config in configure_tasks:
                machine = config["machine"]
                apps = None

                if "apps" in config:
                    apps = config["apps"]

                ret = self.start_service(machine, apps)

                if not ret:
                    print("Failed to start service", machine)
                    return False

            # now services have started run setup
            ret = self.setup()

            if ret is False:
                print("Failed to setup service")
                return False

            # now start tasks
            for config in configure_tasks:
                machine = config["machine"]
                tasks = config["tasks"]
                print("machine", machine, tasks)
                ret = self[machine].configure_tasks(tasks)

                if "x11" in config:
                    self[machine].has_x11 = config["x11"]

                if "no_comm" in config:
                    no_comm = config["no_comm"]
                    for task in self[machine].tasks:
                        for noc in no_comm:
                            if task.name == noc:
                                task.no_comm = True
                                task.is_ready = True

                if not ret:
                    print("Failed to configure tasks", tasks)

            self.connect_tasks = None
            if "connectivity" in graph:
                self.connect_tasks = graph["connectivity"]
        except Exception as e:
            print("Failed to parse ", repr(e))

    def connect(self):

        if self.connect_tasks is None:
            return True

        try:
            for conn_task in self.connect_tasks:
                task = conn_task["task"]
                machine = conn_task["machine"]
                connections = conn_task["connections"]

                for conn in connections:
                    conn_type = conn["type"]
                    freq = 1
                    ip = "localhost"
                    to = conn["to"]
                    to_machine = to.split(":")[0]
                    to_task = to.split(":")[1]
                    tunnel = False

                    if "freq" in conn:
                        freq = conn["freq"]

                    if "ip" in conn:
                        ip = conn["ip"]

                    if "tunnel" in conn:
                        tunnel = conn["tunnel"]

                    print conn_type, freq, ip, to_machine, to_task, tunnel

                    if conn_type == "publish":
                        print "publish", machine, task, self[to_machine][to_task], ip, freq, tunnel
                        result = self[machine][task].publish(self[to_machine][to_task], ip, freq, tunnel)
                        print (result)

                    if conn_type == "push":
                        print "push", machine, task, self[to_machine][to_task], ip, freq, tunnel
                        result = self[machine][task].push(self[to_machine][to_task], ip, freq, tunnel)
                        print (result)

        except Exception as e:
            print("Failed to connect", repr(e))
            return False

        # self.execute()
        return True

    def execute(self):
        # start the demo...
        for m in self.machines:
            for t in m.tasks:
                t.communicate("start")

