__author__ = 'hari'

import protocol
import select
import threading


class DaskProtocol:

    class DaskScheduler(object):
        def __init__(self, client, ip_address, port, password, runscript):
            self.client = client
            self.ip_address = ip_address
            self.port = port
            self.runscript = runscript
            self.password = password
            # self.command = "/usr/common/graphics/visit/camera/runscript.sh {0} {1}".format(ip_address, port)
            self.command = runscript + " {0} {1}".format(ip_address, port)
            # msg.logMessage(self.command,msg.DEBUG)

        def serve(self):
            # msg.logMessage("Serving: " + self.command,msg.INFO)
            channel = self.client.get_transport().open_session()
            # msg.logMessage(channel.get_pty(),msg.DEBUG)
            # channel.exec_command('tty')
            channel.exec_command(self.command)
            ready_to_run = False

            while True:
                if channel.exit_status_ready():
                    break
                rl, wl, xl = select.select([channel], [], [], 10.0)
                if len(rl) > 0:
                    line = channel.recv(1024)
                    #msg.logMessage(line)

                    if 'queued and waiting for resources' in line:
                        # msg.showMessage('Execution queued and waiting for resources...')
                        pass
                    elif 'has been allocated resources' in line:
                        # msg.showMessage('Execution has been allocated resources...')
                        ready_to_run = True
                        pass
                    if ready_to_run and line:
                        # msg.showMessage('Executing job...')
                        ready_to_run = False

                    # if line.find("Password") >= 0:
                    #    #msg.logMessage("writing password",msg.DEBUG)
                    #    channel.sendall(self.password + "\n")

        # msg.logMessage("Ending Dask Scheduler",msg.INFO)

    """
    class DaskWorker(object):

        def __init__(self, client, ip_address, port, nodes, partition, time, processors, threads):
            self.client = client
            self.ip_address = ip_address
            self.port = port
            self.nodes = nodes
            self.partition = partition
            self.time = time
            self.processors = processors
            self.threads = threads

        def start(self):
            # self.command = "/usr/common/graphics/visit/camera/runlocalserver.sh {0} {1}".format(ipaddr, port)
            # self.command = "/usr/common/graphics/visit/camera/runserver.sh {0} {1} {2} {3} {4} {5} {6}".
            # format(ipaddr, port, nodes, partition, time, procs, threads)
            #msg.logMessage(self.command,msg.DEBUG)
            pass

        def serve(self):
            #msg.logMessage("Serving Worker: " + self.command,msg.INFO)
            channel = self.client.get_transport().open_session()
            channel.exec_command(self.command)
            # client.exec_command(self.command)
            while True:
                # if channel.exit_status_ready():
                #  break
                rl, wl, xl = select.select([channel], [], [], 10.0)
                if len(rl) > 0:
                    channel.recv(1024) #TODO: save for debug info?
                    #msg.logMessage("rl:", data,msg.DEBUG)
            #msg.logMessage("Ending Dask Worker",msg.INFO)
    """

    def __init__(self):
        self.connection = protocol.BaseProtocol()
        self.remote_address = None
        self.local_port = None
        self.dask_scheduler = None

        pass

    def connect(self, address, username, password="", ask_pass=False, machine=""):
        self.connection.connect(self, address, username, password, ask_pass)
        self.remote_address = self.connection.get_free_remote_port()
        self.local_port = self.connection.get_free_local_port()
        self.dask_scheduler = None

        #override remote machine with given one...
        if len(machine) > 0:
            self.remote_address[0] = machine

        #msg.logMessage((self.local_port, self.remote_addr),msg.DEBUG)
        #self.start_scheduler(self.remote_address[0], self.remote_address[1], self.client, password, runscript)
        #self.executor = None
        #self.forward_tunnel(self.local_port, self.remote_addr[0], self.remote_addr[1], self.client.get_transport())
        pass

    def connect(self, connection, machine=""):
        self.connection = connection
        self.remote_address = self.connection.get_free_remote_port()
        self.local_port = self.connection.get_free_local_port()
        self.dask_scheduler = None

        #override remote machine with given one...
        if len(machine) > 0:
            self.remote_address[0] = machine

        #msg.logMessage((self.local_port, self.remote_addr),msg.DEBUG)
        #self.start_scheduler(self.remote_address[0], self.remote_address[1], self.client, password, runscript)
        #self.executor = None
        #self.forward_tunnel(self.local_port, self.remote_addr[0], self.remote_addr[1], self.client.get_transport())
        pass

    def execute(self):
        #msg.logMessage("Starting Executor",msg.INFO)
        # self.executor = Executor("{0}:{1}".format(self.remote_addr[0], self.remote_addr[1]))
        #self.executor = Executor("{0}:{1}".format("localhost", self.local_port))
        #msg.logMessage("End Executor",msg.INFO)
        pass

    def close(self):
        #self.client.exec_command("killall dask-scheduler dask-worker")
        #self.client.exec_command("killall " + os.path.basename(self.command))
        pass

    def start(self, ip_address, port, client, password, runscript):
        self.dask_scheduler = DaskProtocol.DaskScheduler(client, ip_address, port, password, runscript)
        server_thread = threading.Thread(target=self.dask_scheduler.serve)
        # server_thread.daemon = True
        server_thread.start()