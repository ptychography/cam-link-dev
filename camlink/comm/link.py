from __future__ import print_function
from machine_profile import *

__author__ = 'hari'

"""
class to connect between two points..
"""


class Link:
    def __init__(self, _source, _destination, is_reverse=False):
        self.source = _source
        self.destination = _destination
        self.source_service = None
        self.source_mp = global_mp[self.source + "-" + self.destination]
        self.is_service_linked = False
        self.is_reverse = is_reverse

    def set_service_link(self, service):
        self.source_service = service
        if self.source_mp is not None:
            print("creating direct connection from ", self.source, " to ", self.destination)
            self.source_service.connect_to(self.source_mp["address"],
                                           self.source_mp["username"],
                                           self.source_mp["password"])
            self.is_service_linked = True

    def create_reverse_link(self):
        """
        """
        return Link(self.destination, self.source, True)

    def is_valid(self):
        if global_mp[self.source] is None or \
           global_mp[self.destination] is None:
            print("Source or Destination is not in global connection list")
            return False

        if global_mp[self.source]["connection"] is None or \
           global_mp[self.destination]["connection"] is None:
            print("Connections have not been established...")
            return False
        return True

    def create_port_reverse(self, source_port=None, dest_port=None):

        if not self.is_valid():
            return None, None

        # create link

        source_conn = global_mp[self.source]["connection"]  # type src.comm.protocol.BaseProtocol
        dest_conn = global_mp[self.destination]["connection"]

        if source_port is None:
            source_port = int(source_conn.get_free_remote_port()[1])

        if dest_port is None:
            dest_port = int(dest_conn.get_free_remote_port()[1])

        self.add_reverse_forward(source_port, dest_port)
        return source_port, dest_port

    def add_reverse_forward(self, source_port, dest_port):

        if not self.is_valid():
            return False

        source_conn = global_mp[self.source]["connection"]  # type src.comm.protocol.BaseProtocol
        dest_conn = global_mp[self.destination]["connection"]

        if self.source == "localhost":
            dest_conn.reverse_forward_tunnel(dest_port, "localhost", source_port) # TEST THIS..
        elif self.destination == "localhost":
            source_conn.forward_tunnel(dest_port, "localhost", source_port)
        #elif self.source_service is not None:
        #    print "here!!"
        #    pass
        else:
            server_port = source_conn.get_free_local_port()
            tmp = source_port
            source_port = dest_port
            dest_port = tmp

            print("creating tunnel from ", source_port, " to ", server_port)
            dest_conn.reverse_forward_tunnel(source_port, "localhost", server_port)

            print("forwarding tunnel from ", server_port, " to ", dest_conn.address, dest_port)
            source_conn.forward_tunnel(server_port, "localhost", dest_port)

            print("linking :", source_port, "->", server_port, "->", dest_port)
        return True

    def create_port_forward(self, source_port=None, dest_port=None):
        """
        :param port:
        :return:
        """

        if not self.is_valid():
            return None, None

        # create link
        dest_conn = global_mp[self.destination]["connection"]  # type src.comm.protocol.BaseProtocol
        source_conn = global_mp[self.source]["connection"]

        if dest_port is None:
            dest_port = int(dest_conn.get_free_remote_port()[1])

        if source_port is None:
            source_port = int(source_conn.get_free_remote_port()[1])

        self.add_forward(source_port, dest_port)
        return source_port, dest_port

    def add_forward(self, source_port, dest_port):

        if not self.is_valid():
            return False

        source_conn = global_mp[self.destination]["connection"]  # type src.comm.protocol.BaseProtocol
        dest_conn = global_mp[self.source]["connection"]

        if self.source == "localhost":
            source_conn.forward_tunnel(source_port, "localhost", dest_port)  # TODO: TEST
        elif self.destination == "localhost":
            dest_conn.reverse_forward_tunnel(dest_port, "localhost", source_port)  # TODO:TEST
        else:
            server_port = source_conn.get_free_local_port()

            # print("creating tunnel from ", source_conn.address, remote_port, " to ", server_port)
            dest_conn.reverse_forward_tunnel(source_port, "localhost", server_port)

            # print("forwarding tunnel from ", server_port, " to ", dest_conn.address, dest_port)
            source_conn.forward_tunnel(server_port, "localhost", dest_port)

            print("linking :", source_port, "->", server_port, "->", dest_port)
        return True

