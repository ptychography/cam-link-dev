from node import *
from link import *


class Graph:
    def __init__(self):
        self.nodes = []
        self.links = []

    def start_connections(self, machines):

        match = False
        for mach in machines:
            for n in self.nodes:
                if n.name == mach:
                    match = True
                    break

            # the node already exists...
            if match:
                print("Machine access already exists in Graph. Skipping..")
                continue

            mp = global_mp[mach]
            print("Processing : ", mach)
            if mp is None:
                print("Configuration entry does not exist ...")
                return False

            if mp["connection"] is None:
                conn = Node()
                self.nodes.append(conn)

                print("Connection from localhost to ", mach)
                if conn.connect(mach):
                    print("Connected to : ", mach)
                else:
                    print(mach, " failed to connect")
            else:
                print("Connected to : ", mach)
                self.nodes.append(mp["connection"])

        return True

    def parse_graph(self, connection_graph):
        # gather unique names
        machines = {}
        for entry in connection_graph:
            machines[entry["name"]] = entry["name"]
            connections = entry["connections"]
            for conn in connections:
                machines[conn] = conn

        configured = True

        for mach in machines.keys():
            mp = global_mp[mach]
            print("Processing : ", mach)
            if mp is None:
                print("Configuration entry does not exist ...")
                configured = False
                break

            if mp["connection"] is None:
                conn = Node()
                self.nodes.append(conn)

                print("Connection from localhost to ", mach)
                if conn.connect(mach):
                    print("Connected to : ", mach)
                else:
                    print(mach, " failed to connect")
            else:
                print("Connected to : ", mach)
                self.nodes.append(mp["connection"])

        if configured is False:
            print("Failed to configure connection. Exiting.")

        # create links
        for link in connection_graph:
            name = link["name"]
            link_connections = link["connections"]
            for link_conn in link_connections:
                link = Link(name, link_conn)
                self.links.append(link)

    def get_links(self, node):
        if isinstance(node, str):
            name = node
        else:
            name = node.name
        links = []
        for l in self.links:
            if l.source == name:
                links.append(l)
        return links

    def get_link(self, source, dest):
        for link in self.links:
            if link.source == source and link.destination == dest:
                return link
            elif link.source == dest and link.destination == source:
                return link.create_reverse_link()
        return None
