import os
import json

__author__ = 'hari'


class OtpPassword:
    def __init__(self, pin, secret):
        self.pin = pin
        self.secret = secret

    @staticmethod
    def which(program):
        import os

        def is_exe(xpath):
            return os.path.isfile(xpath) and os.access(xpath, os.X_OK)

        fpath, fname = os.path.split(program)
        if fpath:
            if is_exe(program):
                return program
        else:
            for path in os.environ["PATH"].split(os.pathsep):
                path = path.strip('"')
                exe_file = os.path.join(path, program)
                if is_exe(exe_file):
                    return exe_file

        return None

    def __call__(self, *args, **kwargs):
        import otp
        result = (self.pin + otp.TOTP(self.secret).now()).strip()
        return result


class MachineProfile:
    def __init__(self):
        self.config = {}
        self.machine_paths = []

        # read machine
        self.read_machine_list()

    @staticmethod
    def get_password(message):
        import getpass
        password = getpass.getpass(prompt="Enter Password for {0}@{1}:{2} :".format(message))
        return password

    def add_machine_profile(self,
                            name,
                            address,
                            port,
                            username,
                            config_dir,
                            password=get_password,
                            password_type=None,
                            environment=None,
                            connection=None,
                            parent=None,
                            use_ncat=False):

        self.config[name] = dict(name=name,
                                 address=address,
                                 port=port,
                                 username=username,
                                 password=password,
                                 password_type=password_type,
                                 config_dir=config_dir,
                                 parent=parent,
                                 environment=environment,
                                 connection=connection,
                                 use_ncat=use_ncat,
                                 history=None)

        # print self.config[name]

    def __getitem__(self, name):
        if name in self.config:
            return self.config[name]
        return None

    def read_machine_list(self, machine_path=None):
        """

        :param machine_path:
        :return:
        """

        if machine_path is None:
            dirname = os.environ["HOME"]
            machine_path = dirname + os.path.sep + "master_connections.txt"

        machine_path = os.path.abspath(os.path.expanduser(machine_path))

        if not os.path.exists(machine_path):
            print("No default connections detected in", machine_path)
            return False

        if machine_path in self.machine_paths:
            print("Machine file name already added skipping..")
            return True

        f = open(machine_path).read()
        result = json.loads(f)

        for mp in result:
            self.parse_json_entry(mp)

        self.machine_paths.append(machine_path)
        return True

    def parse_json_entry(self, mp):
        # unique name key must exist
        if "name" not in mp:
            return False

        name = mp["name"]
        parent = None
        address = None
        port = None
        username = None
        config_dir = None
        password = None
        environment = None
        password_type = None

        try:

            if "machine_access_file" in mp:
                self.read_machine_list(mp["machine_access_file"])
                return

            if "parent" in mp:
                parent = mp["parent"]

                if parent not in self.config:
                    print("Parent entry found, but not parsed. Skipping")
                    return False

                parent_mp = self.config[parent]

                # default to parent information
                address = parent_mp["address"]
                port = parent_mp["port"]
                username = parent_mp["username"]
                config_dir = parent_mp["config_dir"]
                password = parent_mp["password"]
                environment = parent_mp["environment"]

            if "username" in mp:
                username = mp["username"]

            if "address" in mp:
                address = mp["address"]

            if "port" in mp:
                port = mp["port"]

            if "config_dir" in mp:
                config_dir = mp["config_dir"]

            if "environment" in mp:
                environment = mp["environment"]

            if "password" in mp:
                password = mp["password"]

                if password.startswith("otp:") or \
                   password.startswith("otp_with_pin:") or \
                   password.startswith("keyfile:"):
                    sp = password.split(":", 1)
                    password_type = sp[0]
                    password = sp[1]

            if "password_type" in mp:
                password_type = mp["password_type"]

            if password_type is not None:
                if password_type == "otp":
                    password = OtpPassword("", password)

                if password_type == "otp_with_pin":
                    pwd = password.split(":", 1)
                    password = OtpPassword(pwd[0], pwd[1])

                if password_type == "keyfile":
                    password = "keyfile:" + password

                # else it is a regular password...

            use_ncat = False
            if "use_ncat" in mp:
                use_ncat = mp["use_ncat"]

            self.add_machine_profile(name=name,
                                     address=address,
                                     port=port,
                                     username=username,
                                     password=password,
                                     password_type=password_type,
                                     config_dir=config_dir,
                                     environment=environment,
                                     parent=parent,
                                     use_ncat=use_ncat)
        except:
            print("Failed to parse", name)
            return False

        return True


global_mp = MachineProfile()
