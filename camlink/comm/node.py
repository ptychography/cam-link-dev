import os
import getpass
import socket
import select
import threading
import SocketServer
import paramiko
import machine_profile
import shutil
import stat
import errno
import subprocess
import StringIO
import scp
import time

try:
    import logging
    logging.getLogger("paramiko.transport").disabled = True
except:
    print "LOGGING ERROR..."
    pass


class X11handler:
    def __init__(self):
        pass

    def run(self, transport, session, command):
        import Xlib.support.connect as xlib_connect
        import sys

        def x11_handler(channel, (src_addr, src_port)):
            x11_fileno = channel.fileno()
            local_x11_channel = xlib_connect.get_socket(*local_x11_display[:3])
            local_x11_fileno = local_x11_channel.fileno()

            # Register both x11 and local_x11 channels
            channels[x11_fileno] = channel, local_x11_channel
            channels[local_x11_fileno] = local_x11_channel, channel

            poller.register(x11_fileno, select.POLLIN)
            poller.register(local_x11_fileno, select.POLLIN)

            transport._queue_incoming_channel(channel)

        def flush_out(channel):
            while channel.recv_ready():
                sys.stdout.write(channel.recv(4096))
            while channel.recv_stderr_ready():
                sys.stderr.write(channel.recv_stderr(4096))

        local_x11_display = xlib_connect.get_display(os.environ['DISPLAY'])

        channels = {}
        poller = select.poll()
        session_fileno = session.fileno()
        poller.register(session_fileno)

        session.request_x11(handler=x11_handler)
        session.exec_command(command)
        transport.accept()

        # event loop
        while not session.exit_status_ready():
            poll = poller.poll()
            if not poll:  # this should not happen, as we don't have a timeout.
                break

            for fd, event in poll:
                if fd == session_fileno:
                    flush_out(session)
                # data either on local/remote x11 channels/sockets
                if fd in channels.keys():
                    sender, receiver = channels[fd]
                    try:
                        receiver.sendall(sender.recv(4096))
                    except:
                        sender.close()
                        receiver.close()
                        channels.remove(fd)

        flush_out(session)
        return session.recv_exit_status()


class Node:
    class ForwardServer(SocketServer.ThreadingTCPServer):
        """
        Class to support Forwarding through Daemon Threads
        """
        daemon_threads = True
        allow_reuse_address = True

    class Handler(SocketServer.BaseRequestHandler):

        def setup(self):
            # print "setup called"
            pass

        # noinspection PyUnresolvedReferences
        def handle(self):
            """
            """
            try:
                chan = self.ssh_transport.open_channel('direct-tcpip',
                                                       (self.chain_host, self.chain_port),
                                                       self.request.getpeername())
            except Exception as e:
                # TODO: deal with ZMQ connections
                # Node.verbose('Incoming request to %s:%d failed: %s' % (self.chain_host,
                #                                                       self.chain_port,
                #                                                       repr(e)))
                return

            if chan is None:
                # Node.verbose('Incoming request to %s:%d was rejected by the SSH server.' %
                #             (self.chain_host, self.chain_port))
                return

            # Node.verbose('Connected!  Tunnel open %r -> %r -> %r' % (self.request.getpeername(),
            #                                                         chan.getpeername(),
            #                                                         (self.chain_host, self.chain_port)))

            try:
                while True:
                    r, w, x = select.select([self.request, chan], [], [])
                    if self.request in r:
                        data = self.request.recv(1024)
                        if len(data) == 0:
                            break
                        chan.send(data)
                    if chan in r:
                        data = chan.recv(1024)
                        if len(data) == 0:
                            break
                        self.request.send(data)

                chan.close()
                self.request.close()

                peer_name = self.request.getpeername()
                Node.verbose('Forward Tunnel closed from %r' % (peer_name,))
            except:
                pass

        def finish(self):
            # print "Finishing"
            pass

    @staticmethod
    def reverse_handler(chan, host, port):
        sock = socket.socket()
        try:
            sock.connect((host, port))
        except Exception as e:
            # Node.verbose('Forwarding request to %s:%d failed: %r' % (host, port, e))
            return

        Node.verbose('Connected!  Tunnel open %r -> %r -> %r' % (chan.origin_addr,
                                                                 chan.getpeername(), (host, port)))
        while True:
            r, w, x = select.select([sock, chan], [], [])
            if sock in r:
                data = sock.recv(1024)
                if len(data) == 0:
                    break
                chan.send(data)
            if chan in r:
                data = chan.recv(1024)
                if len(data) == 0:
                    break
                sock.send(data)
        chan.close()
        sock.close()
        Node.verbose('Reverse Tunnel closed from %r' % (chan.origin_addr,))

    class ReverseHandler:
        def __init__(self):
            self.ports = dict()

        def my_handler(self, channel, src_addr, dest_addr_port):
            src_port = dest_addr_port[1]
            if src_port in self.ports:
                dest_host = self.ports[src_port][0]
                dest_port = self.ports[src_port][1]
                thr = threading.Thread(target=Node.reverse_handler,
                                       args=(channel, dest_host, dest_port))
                thr.setDaemon(True)
                thr.start()

    """
    Connection class that handles to connect from and to machines
    Services such as Dask & ZMQ can use this to establish connection
    """

    g_verbose = True

    @staticmethod
    def verbose(s):
        """
        All messages come through here
        """
        if Node.g_verbose:
            # logging.Logger.info(msg=s)
            print s

    def __init__(self):
        """
        """
        self.address = None
        self.username = None
        self.password = None
        self.password_type = None
        self.port = None
        self.ask_pass = None
        self.client = None
        self.name = None
        self.config_env = None
        self.local_comm_port = None
        self.remote_comm_port = None
        self.reverse_handler = Node.ReverseHandler()
        self.mp = None
        self.scp = None
        self.sleep_buffer = False

    @staticmethod
    def client_connect(client, address, username, password, port, pkey=None):

        if pkey is not None:
            client.connect(hostname=address, username=username, port=port, pkey=pkey)
            return True

        if callable(password) or hasattr(password, "__call__"):
            password = password()

        if password is None:
            password = getpass.getpass(prompt="Enter Password for {0}@{1}:{2} :".format(username,
                                                                                        address,
                                                                                        port))
            client.connect(hostname=address, username=username, password=password, port=port)
        elif len(password) > 0 and password.startswith("keyfile") is False:
            # print username + "@" + address + ":" + str(port) + ", " + password
            client.connect(hostname=address, username=username, password=password, port=port)
        elif password.startswith("keyfile"):
            key_file = password.split(":")
            client.connect(hostname=address, username=username, port=port, key_filename=key_file[1])
        else:
            client.connect(hostname=address, username=username, port=port)

        return True

    def direct_connect(self, address, username, password, port=22):

        try:
            client = paramiko.SSHClient()
            client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            client.load_system_host_keys()

            if password is None:
                password = getpass.getpass(prompt="Enter Password for {0}@{1}:{2} :".format(username,
                                                                                            address,
                                                                                            port))
                client.connect(hostname=address, username=username, password=password, port=port)
            elif len(password) > 0:
                client.connect(hostname=address, username=username, password=password, port=port)
            else: # blank password is treated like no password
                client.connect(hostname=address, username=username, port=port)
        except Exception as e:
            print repr(e)
            return False
        # finally:
        #    client.close()

        self.address = address
        self.username = username
        self.password = password
        self.port = port
        self.client = client
        return True

    def connect(self, name, username=None, password=None, port=22):
        """
        :param name:
        :param username:
        :param password:
        :param port:
        :return:
        """

        mp = machine_profile.global_mp[name]
        self.mp = mp

        # machine profile not found return False
        if mp is None:
            print("Machine Profile " + name + " was not found.")
            return False

        # connection already exists
        if mp["connection"] is not None:
            print("Connection to this machine already exists")
            return True

        address = mp["address"]

        if username is None or len(username) == 0:
            username = mp["username"]

        if mp["parent"] is not None:
            parent_mp = machine_profile.global_mp[mp["parent"]]

            if parent_mp["connection"] is None:
                p = Node()
                if not p.connect(mp["parent"]):
                    print("Parent unable to connect ", parent_mp)
                    return False

            # modify port
            # since success parent_mp should have created a connection
            port = self.get_free_local_port()
            parent_mp["connection"].forward_tunnel(port, address, 22)
            address = "localhost"

        if password is None:
            password = mp["password"]

        pkey = None

        # if has parent and has keyfile
        # need to read it...
        if mp["parent"] is not None:
            if isinstance(password, str) or isinstance(password, unicode):
                if password.startswith("keyfile"):
                    # read keyfile from remote source
                    try:
                        pmp = machine_profile.global_mp[mp["parent"]]
                        keysplit = password.split(":", 1)
                        cmd = "/bin/cat " + str(keysplit[1])
                        results = pmp["connection"].execute(cmd)
                        results = "".join(results)
                        fake_file = StringIO.StringIO(results)

                        if results.find("BEGIN DSA PRIVATE") > 0:
                            pkey = paramiko.DSSKey.from_private_key(fake_file)
                        elif results.find("BEGIN DSA PRIVATE") > 0:
                            pkey = paramiko.RSAKey.from_private_key(fake_file)
                            print pkey
                    except Exception as e:
                        print("Keyfile parsing did not work...", repr(e))
                        return False

        #print "password = ", password

        #try:
        client = paramiko.SSHClient()
        client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        client.load_system_host_keys()

        connected = False
        tries = 0
        while not connected and tries < 3:
            try:
                self.client_connect(client, address, username, password, port, pkey=pkey)
                connected = True
            except:
                tries += 1
                print("Connection failed, trying again in 5 seconds({0}/3)".format(tries))
                import time
                time.sleep(5)

        self.name = name
        self.address = address
        self.username = username
        self.password = password
        self.port = port
        self.client = client
        self.config_env = mp["config_dir"]

        # read environment file..
        #if len(self.config_env) > 0:
        #    ftp = self.client.open_sftp()
        #    ftp.get(self.config_env + "/setup_env")
        #    ftp.close()

        mp["connection"] = self

        #except Exception as e:
        #    print repr(e)
        #    return False

        return True

    def path_exists(self, path):
        """
        os.path.exists for paramiko's SCP object
        """

        # TODO: REPLACE python with something more generic

        try:
            cmd = "python -c \"import os.path; print os.path.exists(\'{0}\')\"".format(path)
            # print "checking command..", cmd
            result = self.execute(cmd)
            # print result
            if result[0].strip() == "True":
                return True
            else:
                return False
        except Exception as e:
            print("Exception", repr(e))
            pass

        # try again..
        try:
            sftp = self.client.open_sftp()
            ret = True
            sftp.stat(path)
            sftp.close()
        except Exception as e:
            ret = False
            # print("Path check failed with", repr(e))

        return ret

    def put_dir(self, source_directory, remote_directory):

        try:

            if os.path.basename(remote_directory) == os.path.basename(source_directory) \
                    and self.path_exists(remote_directory):
                remote_directory = os.path.dirname(remote_directory)

            self.mkdir(os.path.dirname(remote_directory))

            if self.sleep_buffer:
                time.sleep(2)

            transport = self.client.get_transport()
            scpclient = scp.SCPClient(transport)
            scpclient.put(source_directory, remote_directory, True)
            scpclient.close()
            return True
        except:
            pass

        # if scpclient fails try open_sftp option
        sftp = self.client.open_sftp()
        print("Copying:", source_directory, "to", remote_directory)
        self.mkdir_p(sftp, remote_directory)
        self.put_dir_p(sftp, source_directory, remote_directory)
        sftp.close()

    def put_dir_p(self, sftp, source, target):
        """
            Uploads the contents of the source directory to the target path. The
            target directory needs to exists. All subdirectories in source are
            created under target.
        """

        for item in os.listdir(source):
            if os.path.isfile(os.path.join(source, item)):
                # print (os.path.join(source, item), '%s/%s' % (target, item))
                sftp.put(os.path.join(source, item), '%s/%s' % (target, item))
            else:
                self.mkdir_p(sftp, '%s/%s' % (target, item))
                self.put_dir_p(sftp, os.path.join(source, item), '%s/%s' % (target, item))

    def put(self, filename, remote_directory):
        import os

        try:
            if self.sleep_buffer:
                time.sleep(2)

            transport = self.client.get_transport()
            scpclient = scp.SCPClient(transport)
            scpclient.put(filename, remote_directory)
            scpclient.close()
            return True
        except:
            pass

        dest_filename = os.path.basename(filename)
        sftp = self.client.open_sftp()
        sftp.chdir(remote_directory)
        sftp.put(filename, dest_filename)
        sftp.close()

    def mkdir(self, remote_directory):

        try:
            cmd = "python -c  \"import os; path = '{0}'; os.makedirs(path) if not os.path.exists(path) else '' \"".format(remote_directory)
            print cmd
            self.execute(cmd)
            return
        except Exception as e:
            print(e)
            pass

        sftp = self.client.open_sftp()
        self.mkdir_p(sftp, remote_directory)
        sftp.close()

    def mkdir_p(self, sftp, remote_directory):
        """Change to this directory, recursively making new folders if needed.
        Returns True if any folders were created."""
        import os.path

        if remote_directory == '/':
            # absolute path so change directory to root
            sftp.chdir('/')
            return True
        if remote_directory == '':
            # top-level relative directory must exist
            return True
        try:
            sftp.chdir(remote_directory)  # sub-directory exists
        except IOError:
            dir_name, basename = os.path.split(remote_directory.rstrip('/'))
            self.mkdir_p(sftp, dir_name)  # make parent directories
            sftp.mkdir(basename)  # sub-directory missing, so created it
            sftp.chdir(basename)
        return True

    def exec_command(self, command, bufsize=-1, timeout=None, get_pty=False, request_x11=False):
        """
        Execute a command on the SSH server.  A new `.Channel` is opened and
        the requested command is executed.  The command's input and output
        streams are returned as Python ``file``-like objects representing
        stdin, stdout, and stderr.

        :param str command: the command to execute
        :param int bufsize:
            interpreted the same way as by the built-in ``file()`` function in
            Python
        :param int timeout:
            set command's channel timeout. See `Channel.settimeout`.settimeout
        :return:
            the stdin, stdout, and stderr of the executing command, as a
            3-tuple

        :raises SSHException: if the server fails to execute the command
        """
        chan = self.client.get_transport().open_session()
        try:
            chan.request_x11()
            print "X Forwarding requested"
            if get_pty:
                chan.get_pty()
            chan.settimeout(timeout)

            chan.exec_command(command)
            stdin = chan.makefile('wb', bufsize)
            stdout = chan.makefile('r', bufsize)
            stderr = chan.makefile_stderr('r', bufsize)
            chan.close()
            return stdin, stdout, stderr
        except:
            print "X Forwarding disabled"
            pass

        chan = self.client.get_transport().open_session()

        if get_pty:
            chan.get_pty()
        chan.settimeout(timeout)
        chan.exec_command(command)
        stdin = chan.makefile('wb', bufsize)
        stdout = chan.makefile('r', bufsize)
        stderr = chan.makefile_stderr('r', bufsize)
        chan.close()
        return stdin, stdout, stderr

    def execute(self, command, environment=None, input_params=None, request_x11=False):
        """
        :param command:
        :param input_params:
        :return:
        """

        try:
            std_in, std_out, std_err = self.client.exec_command(command)

            if input_params is not None:
                std_in.write(input_params + "\n")
                std_in.flush()

            std_in.close()

            result = std_out.readlines()
            return result
        except paramiko.ChannelException as e:
            if e.message == "Administratively prohibited":
                time.sleep(2)

            # try again after sleeping for 2 seconds...
            std_in, std_out, std_err = self.client.exec_command(command)

            if input_params is not None:
                std_in.write(input_params + "\n")
                std_in.flush()

            std_in.close()

            result = std_out.readlines()
            return result

    def write_to_file(self, content, filename):

        try:
            print os.path.basename(filename), os.path.dirname(filename)
            new_filename = "/tmp/" + os.path.basename(filename)
            new_file = open(new_filename, "w")
            new_file.write(content)
            new_file.close()

            res = self.put(new_filename, os.path.dirname(filename))
            os.remove(new_filename)
            return res
            # time.sleep(2)
            # transport = self.client.get_transport()
            # scpclient = scp.SCPClient(transport)
            # scpclient.put_stream(content, os.path.basename(filename), os.path.dirname(filename))
            # scpclient.close()
            # return True
        except Exception as e:
            print("WRITING FAILED: ", repr(e))
            pass

        """
        try:
            print os.path.basename(filename), os.path.dirname(filename)
            time.sleep(2)
            transport = self.client.get_transport()
            scpclient = scp.SCPClient(transport)
            scpclient.put_stream(content, os.path.basename(filename), os.path.dirname(filename))
            scpclient.close()
            return True
        except:
            pass
        """

        ftp = self.client.open_sftp()
        file = ftp.file(filename, "w")
        file.write(content)
        file.flush()
        ftp.close()

    def iexecute(self, command):
        """
        Interactive execute
        :param command:
        :return:
        """
        if self.sleep_buffer:
            time.sleep(2)

        output = ""
        transport = self.client.get_transport()
        channel = transport.open_session()
        channel.exec_command(command)

        while True:
            rl, wl, xl = select.select([channel], [], [], 0.0)
            if len(rl) > 0:
                data = channel.recv(1024)

                if data is None or len(data) == 0:
                    break

                data = data.strip()

                if len(data) > 2:
                    print(data)
                    output += data

        channel.close()
        if self.sleep_buffer:
            time.sleep(2)

        return output

    def bg_execute(self, command, output_callback=None):
        """
        :param command:
        :param output_callback
        :return:
        """

        if self.sleep_buffer:
            time.sleep(2)

        transport = self.client.get_transport()
        channel = transport.open_session()

        # try:
        #    channel.request_x11()
        #    channel.get_pty()
        #    print "X FORWARDING ACCEPTED..."
        #except:
        #    channel = transport.open_session()
        #    print "X FORWARDING REJECTED..."

        def background(chan, _command):
            chan.exec_command(_command + "\n")
            while True:
                try:
                    rl, wl, xl = select.select([chan], [], [], 0.0)
                    if output_callback is not None and rl is not None and len(rl) > 0:
                        res = chan.recv(1024)
                        res = res.strip()
                        if len(res) > 0:
                            output_callback(res)
                except:
                    print("Exiting background process thread")
                    chan.close()
                    break

        t = threading.Thread(target=background, args=(channel, command))
        t.daemon = True
        t.start()

    def bg_x11_execute(self, command, output_callback=None):
        """
        :param command:
        :param output_callback
        :return:
        """

        import Xlib.support.connect as xlib_connect
        import sys

        channelOppositeEdges = {}
        local_x11_display = xlib_connect.get_display(os.environ['DISPLAY'])
        inputSockets = []

        transport = self.client.get_transport()
        channel = transport.open_session()

        def background(transport, session, _command):
            def x11_handler(channel, (src_addr, src_port)):
                local_x11_socket = xlib_connect.get_socket(*local_x11_display[:3])
                inputSockets.append(local_x11_socket)
                inputSockets.append(channel)
                channelOppositeEdges[local_x11_socket.fileno()] = channel
                channelOppositeEdges[channel.fileno()] = local_x11_socket
                transport._queue_incoming_channel(channel)

            inputSockets.append(session)
            session.request_x11(handler=x11_handler)
            session.exec_command(command)
            transport.accept()

            while not session.exit_status_ready():
                readable, writable, exceptional = select.select(inputSockets, [], [])
                if len(transport.server_accepts) > 0:
                    transport.accept()
                for sock in readable:
                    if sock is session:
                        while session.recv_ready():
                            sys.stdout.write(session.recv(4096))
                        while session.recv_stderr_ready():
                            sys.stderr.write(session.recv_stderr(4096))
                    else:
                        try:
                            data = sock.recv(4096)
                            counterPartSocket = channelOppositeEdges[sock.fileno()]
                            counterPartSocket.sendall(data)
                        except socket.error:
                            inputSockets.remove(sock)
                            inputSockets.remove(counterPartSocket)
                            del channelOppositeEdges[sock.fileno()]
                            del channelOppositeEdges[counterPartSocket.fileno()]
                            sock.close()
                            counterPartSocket.close()

            print 'Exit status:', session.recv_exit_status()
            while session.recv_ready():
                sys.stdout.write(session.recv(4096))
            while session.recv_stderr_ready():
                sys.stdout.write(session.recv_stderr(4096))
            session.close()

        t = threading.Thread(target=background, args=(transport, channel, command))
        t.daemon = True
        t.start()

    @staticmethod
    def get_free_local_port():
        """
        :return:
        """
        s = socket.socket()
        s.bind(("", 0))
        local_port = s.getsockname()[1]
        s.close()
        return local_port

    def get_free_remote_port(self):
        """
        :return:
        """

        #if self.mp["use_ncat"]:
        #    return machine_profile.global_mp[self.mp["parent"]]["connection"].get_free_remote_port()

        # TODO: THIS NEEDS TO KNOW ABOUT Python
        if self.path_exists(self.config_env + "/conda/bin/python"):
            command = self.config_env + "/conda/bin/python"
            command += " -c 'import socket; s=socket.socket(); s.bind((\"\", 0));"
            command += " result = socket.gethostbyname(socket.gethostname()) + \":\" + str(s.getsockname()[1]);"
            command += " s.close(); print(result)'"
        else:
            command = "python -c 'import socket; s=socket.socket(); s.bind((\"\", 0));"
            command += " result = socket.gethostbyname(socket.gethostname()) + \":\" + str(s.getsockname()[1]);"
            command += " s.close(); print(result)'"

        result = self.execute(command)[0].strip()
        remote_address = result.split(":")
        return remote_address

    def forward_tunnel(self, local_port, remote_host, remote_port, transport=None):
        """
        :param local_port:
        :param remote_host:
        :param remote_port:
        :param transport:
        :return:
        """
        if "use_ncat" in self.mp and  self.mp["use_ncat"]:
            print "Forwarding..."
            parent_mp = machine_profile.global_mp[self.mp["parent"]]
            print local_port, self.address, self.mp["address"], remote_port
            parent_mp["connection"].forward_tunnel(local_port, self.mp["address"], remote_port)
            return

        class SubHandler(Node.Handler):
            chain_host = remote_host
            chain_port = int(remote_port)
            if transport is None:
                ssh_transport = self.client.get_transport()
            else:
                ssh_transport = transport

        #try:
        fs = Node.ForwardServer(('', int(local_port)), SubHandler)
        # msg.logMessage(("FORWARDING: ", local_port, remote_host, remote_port),msg.INFO)
        server_thread = threading.Thread(target=fs.serve_forever)
        server_thread.daemon = True
        server_thread.start()
        #except:
        #    pass
        return None

    def reverse_forward_tunnel(self, server_port, remote_host, remote_port, transport=None):

        # should localhost to localhost tunnelling be allowed?
        if "use_ncat" in self.mp and  self.mp["use_ncat"]:
            # print "Reverse Forwarding...", server_port, remote_host, remote_port,
            parent_mp = machine_profile.global_mp[self.mp["parent"]]
            remote_middle_port = parent_mp["connection"].get_free_remote_port()
            print remote_middle_port
            remote_middle_port = remote_middle_port[1]

            parent_mp["connection"].reverse_forward_tunnel(server_port, "localhost", remote_port)
            # parent_mp["connection"].reverse_forward_tunnel(remote_middle_port, "localhost", remote_port)

            print "SERVER:", server_port, remote_middle_port, remote_port

            cmd = "rm -fR /tmp/backpipe && mkfifo /tmp/backpipe && "
            cmd += "nc -k -l " + str(remote_middle_port) + " 0</tmp/backpipe | nc localhost "
            cmd += str(server_port) + " 1>/tmp/backpipe"
            print cmd
            # parent_mp["connection"].bg_execute(cmd)
            return remote_middle_port

        if transport is None:
            transport = self.client.get_transport()

        #try:
        self.reverse_handler.ports[server_port] = [remote_host, remote_port]
        transport.request_port_forward('', server_port, self.reverse_handler.my_handler)
        #except:
        #    pass
        return None
