import os
import sys
import camlink
from camlink.services import graph as task_graph

__author__ = 'hari'

graph = None


def main():
    if len(sys.argv) < 2:
        print("filename not given to parse..")
        sys.exit(0)

    filename = sys.argv[1]

    if not os.path.exists(filename):
        print(filename, "not a proper file or file does not exist")
        sys.exit(0)

    print("Processing: ", filename)

    global graph
    graph = task_graph.Graph()
    graph.parse(filename)


def start():
    global graph
    graph.start_tasks()
    graph.connect()
    graph.execute()


def main_non_interactive():
    import time
    import camlink.services.service
    path = os.path.dirname(camlink.__file__) + "/../"
    print("APP", path)
    camlink.services.service.Service.app_dirname = path
    global graph

    main()
    start()

    while True:
        time.sleep(5)


if __name__ == "__main__":
    main()
