from camlink.services import graph as task_graph
from camlink.comm.machine_profile import global_mp

__author__ = 'hari'

global_vis_ports = [0, 0]
graph = None


def start_network():
    global graph, global_vis_ports

    global_vis_ports[0] = graph["localhost"]["cosmic"].publish(graph["localhost"]["vis"], "localhost", 5, tunnel=False)
    graph["localhost"]["cosmic"].push(graph["localhost"]["sharp"], "localhost", 1, tunnel=False)
    global_vis_ports[1] = graph["localhost"]["sharp"].publish(graph["localhost"]["vis"], "localhost", 5, tunnel=False)

    graph["localhost"]["sharp"].communicate("start")
    graph["localhost"]["cosmic"].communicate("start")
    return global_vis_ports


def start_vis_network():
    global graph, global_vis_ports
    vis_network = [{"type": "subscribe", "ip": "localhost", "port": global_vis_ports[0]},
                   {"type": "subscribe", "ip": "localhost", "port": global_vis_ports[1]}]

    graph["localhost"]["vis"].communicate(vis_network)
    graph["localhost"]["vis"].communicate("start")


def main():
    global graph
    global_mp.add_machine_profile(name="localhost",
                                  address="localhost",
                                  port=22,
                                  username="hari",
                                  password="keyfile:/Users/hari/.ssh/id_rsa",
                                  config_dir="/tmp/camera")

    graph = task_graph.Graph()

    machine_list = [
        {
            "machine": "localhost",
            "apps": [
                {
                    "name": "nanosurveyor/vis-wrapper"
                },
                {
                    "name": "nanosurveyor/sharp-wrapper",
                    "template": {"ROOT_BIN_PATH": "/usr/local/"}
                 },
                {
                    "name": "nanosurveyor/sharp-worker",
                    "template": {"ROOT_BIN_PATH": "/usr/local/"}
                },
                {
                    "name": "nanosurveyor/cosmic-wrapper",
                    "template":
                        {
                            "ROOT_BIN_PATH": "/usr/local/",
                            "PARAM_FILE": "/Users/hari/test_data_sets/161102035/001/param.txt"
                        }
                 }
            ]
        }
    ]

    result = graph.start_services(machine_list)

    if result is True:
        print("Services started successfully to: ", machine_list)

    result = graph.setup()

    if result is True:
        print("Services setup successfully to: ", machine_list)

    for machine in graph.machines:
        for service in machine.all_services().values():
            print("Available: ", machine.name, service.name(), service.input_vars(), service.output_vars())

    graph["localhost"].configure_tasks(["vis", "cosmic", "sharp", "sharp-worker"])
    graph["localhost"].has_x11 = True

def start():
    global graph
    graph.start_tasks()
    start_network()
    start_vis_network()

if __name__ == "__main__":
    main()
