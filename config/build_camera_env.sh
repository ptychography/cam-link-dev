#!/bin/bash

CAM_DIR=$1

if [[ -z "${CAM_DIR// }"  ]]; then
  CAM_DIR=$PWD
fi

cd $CAM_DIR

unamestr=`uname`
download_prefix='https://repo.continuum.io/miniconda'
download_file=''

if [[ "$unamestr" == 'Linux' ]]; then
   download_file='Miniconda2-latest-Linux-x86_64.sh'
elif [[ "$unamestr" == 'Darwin' ]]; then
   download_file='Miniconda2-latest-MacOSX-x86_64.sh'
else
   printf "Unsupported format $unamestr. Aborting...\n"
fi

if [ -d $CAM_DIR/conda ]; then
  printf "Conda directory already present. Skipping...\n"
  source $CAM_DIR/conda/bin/activate
  source $CAM_DIR/conda/bin/activate
  py=`which python`
  co=`which conda`
  printf "Python at $py, conda at $co\n"
  printf "Finished setting up environment\n"
  printf "__$PWD"
  printf "__$py"
  exit 0
fi

printf "Setting up environment on $PWD\n"
curl -s $download_prefix/$download_file > $download_file
bash $download_file -f -b -p $CAM_DIR/conda
source $CAM_DIR/conda/bin/activate
source $CAM_DIR/conda/bin/activate
py=`which python`
co=`which conda`
printf "Python at $py, conda at $co\n"
conda install -q -y paramiko pyzmq dask distributed

printf "Finished setting up environment\n"
# output to do sanity check..
printf "__$PWD\n"
printf "__$CAM_DIR/conda/bin/python\n"
